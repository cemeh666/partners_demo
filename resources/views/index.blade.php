@extends('layouts.front')

@section('content')
<div class="dots">
    <ul>
        <li><a href="#top" class="circle"></a></li>
        <li><a href="#our_partners" class="circle"></a></li>
        <li><a href="#works" class="circle"></a></li>
        <li><a href="#donate" class="circle"></a></li>
        <li><a href="#statistics" class="circle"></a></li>
        <li><a href="#contacts" class="circle"></a></li>
    </ul>
</div>
@if(Auth::check() && Auth::user()->isSuperAdmin())
    <button id="start_editable" class="btn btn-primary" style="position: fixed;">Start Editor</button>
@endif
<div class="scroll">
    <section class="top" id="top">
        <div class="container">
            <div class="logo">
                <img src="/images/front/logo.svg" alt="Loyal Moving partners">
               {{-- <div class="delimiter"></div> --}}
              {{--  <p>Loyal Moving <br> Partners</p> --}}
                @if(Auth::guest())
                    <div class="login" data-toggle="modal" data-target="#signInModal">
                        <a href="#" id="login"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a>
                    </div>
                    {{--<div class="login auth">--}}
                        {{--<form action="/api/auth" method="post" data-form-auth="true" id="login-form">--}}
                            {{--<input type="email" name="email" placeholder="Email:" required="true">--}}
                            {{--<input type="password" name="password" placeholder="Password:" required="true">--}}
                            {{--<span class="reset-password" data-toggle="modal" data-target="#resetModal">Forget <img src="/images/front/key.png"></span>--}}
                            {{--<button><i class="fa fa-sign-in" aria-hidden="true"></i> Enter</button>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                @endif
            </div>

            <div class="main-text editable" id="text1">
                {!! \App\TextEditor::getText('text1') !!}

            </div>
        </div>
    </section>

    <section class="our_partners editable" id="our_partners">
        {!! \App\TextEditor::getText('our_partners') !!}
    </section>
    <section class="works editable" id="works">
        {!! \App\TextEditor::getText('works') !!}
    </section>

    <section class="donate editable" id="donate">
        {!! \App\TextEditor::getText('donate') !!}
    </section>

    <section class="statistics" id="statistics">
        <div class="container">
            <h2 class="title">
                STATISTICS
            </h2>
            <div class="text-center block">
                <div class="partners">
                    <img src="/images/front/statistics-partners.png" alt="">
                    <p class="count">{{$partners}}</p>
                    <p class="value">partners</p>
                </div>
                <div class="cache-back">
                    <img src="/images/front/statistics-money.png" alt="">
                    {{--<p class="count">$77,358.00</p>--}}
                    <p class="count">${{$cache}}</p>
                    <p class="value">cashback</p>
                </div>
                <div class="donate">
                    <img src="/images/front/statistics-donate.png" alt="">
                    <p class="count">{{$donate}}%</p>
                    <p class="value">donate</p>
                </div>
            </div>
            <div id="statistic_text" class="editable">
                <p class="text" >
                    {!! \App\TextEditor::getText('statistic_text') !!}
                </p>
            </div>

        </div>
    </section>
</div>

@endsection
