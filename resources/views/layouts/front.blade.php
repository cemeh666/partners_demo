<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ (isset($title)) ? $title : 'Partners' }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fixed.css') }}" rel="stylesheet">
    <link href="{{ asset('css/loaders.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fonts/roboto.css') }}" rel="stylesheet">
    <link href="{{ asset('css/calendar.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/froala/froala_editor.css') }}" rel="stylesheet">
    <link href="{{ asset('/froala/froala_style.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('/favicon.ico') }}" type="image/x-icon">
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyDsuYcboa2Go4vI2UK68brEf96pzUU7eMY"></script>
    <style>
        .fr-box.fr-basic.fr-top .fr-wrapper{
            background: transparent;
        }
    </style>
</head>
<body>
@if(Auth::check())

    <header>
        <div class="container">
            <div class="navigate-mobile">
                <a href="#" class="bar" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </a>
                <ul class="auth">
                    <li class="avatar">
                        <a href="/profile">
                            <img src="{{Auth::user()->getAvatar()}}" alt="">
                        </a>
                    </li>
                    <li class="id">
                        ID: {{Auth::user()->getID()}} <a href="/settings"><i class="fa fa-cog settings"></i></a>
                    </li>

                </ul>
                <ul class="navigate collapse" id="navbar">
                    <li @if(stristr($_SERVER['REQUEST_URI'], 'profile')) class="active" @endif>
                        <a href="/profile">home</a>
                    </li>
                    <li @if(stristr($_SERVER['REQUEST_URI'], 'invites')) class="active" @endif>
                        <a href="/invites">invite</a>
                    </li>
                    <li @if(stristr($_SERVER['REQUEST_URI'], 'insurance')) class="active" @endif>
                        <a href="/insurance">insurance</a>
                    </li>
                    {{--<li @if(stristr($_SERVER['REQUEST_URI'], 'chart')) class="active" @endif>--}}
                        {{--<a href="/chart">commission chart</a>--}}
                    {{--</li>--}}

                    <li>
                        <a href="/logout">Log out <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>
            <ul class="navigate">
                <li @if(stristr($_SERVER['REQUEST_URI'], 'profile')) class="active" @endif>
                    <a href="/profile">home</a>
                </li>
                <li @if(stristr($_SERVER['REQUEST_URI'], 'invites')) class="active" @endif>
                    <a href="/invites">invite</a>
                </li>
                <li @if(stristr($_SERVER['REQUEST_URI'], 'insurance')) class="active" @endif>
                    <a href="/insurance">insurance</a>
                </li>
                {{--<li @if(stristr($_SERVER['REQUEST_URI'], 'chart')) class="active" @endif>--}}
                    {{--<a href="/chart">commission chart</a>--}}
                {{--</li>--}}

            </ul>
            <ul class="auth">
                <li class="avatar">
                    <a href="/profile">
                        <img src="{{Auth::user()->getAvatar()}}" alt="">
                    </a>
                </li>
                <li>
                    ID: {{Auth::user()->getID()}} <a href="/settings"><i class="fa fa-cog settings"></i></a>
                </li>
                <li>
                    <a href="/logout">Log out <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                </li>
            </ul>
        </div>
    </header>
    @endif
        @yield('content')
    <section class="contacts" id="contacts">
        <div class="container text-center">
            <h2 class="title">CONTACT US</h2>
            <form action="/send/contact" method="post" class="form form-horizontal">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Name:" required="true">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="address" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email:" required="true">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="phone" placeholder="Phone number:" required="true">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group textarea">
                        <textarea name="comments" id="" cols="30" rows="10" placeholder="Comments:" required="true"></textarea>
                    </div>
                </div>
                <button>SEND</button>
            </form>
            <img src="/images/front/keyboard.png" alt="" class="keyboard">
            <div class="copy">� 2003-2017 LOYAL MOVING PARTNERS. All rights reserved.</div>
        </div>
    </section>
    @include('layouts._modals')
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script src="/js/device.js"></script>
    <script src="/js/jquery.maskedinput.min.js"></script>
    <script src="/js/calendar.0.9.min.js"></script>
    <script src="/js/scroll.js"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script src="{{ asset('js/forms.js') }}"></script>
    <script src="{{ asset('/froala/froala_editor.min.js') }}"></script>
    <script src="{{ asset('/froala/link.min.js') }}"></script>
    <script src="{{ asset('/froala/inline_style.min.js') }}"></script>
    <script src="{{ asset('/froala/save.min.js') }}"></script>


    <script>
        $( document ).ready(function() {

            @if(Session::get('errorMessage'))
                $("#failedModal").modal('show');
            @endif
             @if(Session::get('successMessage'))
                $("#successModal").modal('show');
            @endif
            var editing = 0;
            $('#start_editable').on('click', function () {
                if(!editing){
                    editing = 1;
                    $(this).removeClass().addClass('btn btn-danger').text('Stop Editing');
                    $(function() {
                        $('.editable')
                            .froalaEditor({
                                // Change save interval (time in miliseconds).
                                saveInterval: 1500,

                                // Set the save param.
                                saveParam: 'content',

                                // Set the save URL.
                                saveURL: '/api/save/editable',

                                // HTTP request type.
                                saveMethod: 'POST'

                            })
                            .on('froalaEditor.save.before', function (e, editor) {
                                var edit = $('#'+e.target.id).data('froala.editor');
                                var newOpts = {saveParams: {id: e.target.id}};
                                $.extend(edit.opts, newOpts);
                            })
                            .on('froalaEditor.save.after', function (e, editor, response) {
                                // After successfully save request.
                            })
                            .on('froalaEditor.save.error', function (e, editor, error) {
                                // Do something here.
                            })
                        $('.fr-wrapper').next().remove();
                    });
                }
                else
                    location.reload();
            })


        });
    </script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53217967-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
