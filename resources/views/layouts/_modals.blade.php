
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="errorModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h3><i class="fa fa-4x fa-times-circle text-danger"></i></h3>
            <p>An unexpected error occurred. Contact Support</p>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="successModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h3><i class="fa fa-4x fa-check-circle text-success" aria-hidden="true"></i></h3>
            <p data-success-modal>
                @if(Session::get('successMessage'))
                    {{Session::get('successMessage')}}
                @endif
            </p>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="failedModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <i class="fa fa-info-circle text-primary" aria-hidden="true"></i>
            <h3 class="title">
                Correct the mistakes
            </h3>
            <ul class="error-message">

            </ul>
            @if(Session::get('errorMessage'))
                <ul class="error-message">
                    @foreach(Session::get('errorMessage') as $error)
                        <li><i class="fa fa-close text-danger"></i> {{$error}}</li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
</div>

 {{--Balance modal--}}
@if(Auth::check())
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="balanceModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-balance">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="head">
                    <h2 class="title">balance <span>${{number_format(Auth::user()->balance, 2)}}</span></h2>
                </div>
                <div class="body">
                        <div class="title">Select the amount to be paid:</div>
                        <div class="choice">
                            <div class="btn-group type" data-toggle="buttons">
                                <label class="btn btn-default active">
                                    <input type="radio" name="type" value="1" autocomplete="off" checked class="type-paid">
                                <img src="/images/admin/check.png" alt=""> <span>Check</span>
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" name="type" value="2" autocomplete="off" class="type-paid">
                                    <img src="/images/admin/pp.png" alt=""> <span>PayPal</span>
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" name="type" value="3" autocomplete="off">
                                    <img src="/images/admin/gift.png" alt=""> <span>Donate</span>
                                </label>
                            </div>
                        </div>
                        <div class="container">
                            <div class="paid-check">
                                <form action="/balance/withdrawal/1" method="post" data-form-withdrawal="1">

                                    <div class="form-group">
                                    <label >Your name:</label>
                                    <input type="text" name="name" class="form-control" placeholder="" required="true"
                                           value="{{$user->name}}">
                                    </div>
                                    <div class="form-group">
                                        <label >Address</label>
                                        <input type="text" name="address" class="form-control" placeholder="Street address, PO box, company name" required="true"
                                               value="{{$user->address}}">
                                    </div>

                                    <div class="form-group">
                                        <label >City:</label>
                                        <input type="text" name="city" class="form-control" placeholder=""
                                               value="{{$user->city}}">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label >State/Probince/Region:</label>
                                                <input type="text" name="region" class="form-control" placeholder=""
                                                       value="{{$user->state}}">
                                            </div>
                                            <div class="col-md-6">
                                                <label>ZIP/Postal code:</label>
                                                <input type="text" name="zip" class="form-control" placeholder=""
                                                       value="{{$user->zip}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <label>Count USD:</label>
                                        <input type="text" name="count" class="form-control" placeholder="" >
                                    </div>
                                    <div class="text-center">
                                        <button >Send</button>
                                    </div>
                                </form>
                            </div>
                            <div class="paid-paypal">
                                <form action="/balance/withdrawal/2" method="post" data-form-withdrawal="1">

                                    <div class="form-group">
                                        <label >Email:</label>
                                        @if($user->paypal)
                                            <input type="email" name="email" class="form-control" placeholder="" required="true"
                                               value="{{$user->paypal}}">
                                        @else
                                        <input type="email" name="email" class="form-control" placeholder="" required="true"
                                               value="{{$user->email}}">
                                        @endif
                                    </div>
                                    <div class="form-group" >
                                        <label>Count USD:</label>
                                        <input type="text" name="count" class="form-control" placeholder="" >
                                    </div>
                                    <div class="text-center">
                                        <button >Send</button>
                                    </div>
                                </form>
                            </div>
                            <div class="paid-donate">
                                <form action="/balance/withdrawal/3" method="post" data-form-withdrawal="1">

                                    <div class="row">
                                        <p class="text-center title">Choose The Charity To Donate</p>
                                        @foreach(\App\Donate::getDonate() as $key => $donate)
                                            <label class="form-group donate">
                                                <div class="donate-company @if($key==0) active @endif">
                                                    <img src="{{$donate->image}}" alt="" class="donate-img">
                                                    <input type="radio" name="donate" value="{{$donate->id}}" autocomplete="off" @if($key==0) checked @endif class="d-n">
                                                </div>
                                            </label>
                                        @endforeach
                                    </div>
                                    <div class="form-group mg-t" >
                                        <label>Custom:</label>
                                        <input type="text" name="custom_name" id="custom_name" class="form-control" placeholder="Name of organisation">
                                    </div>
                                    <div class="form-group" >
                                        <label>Your name:</label>
                                        <input type="text" name="name" class="form-control" placeholder="Your name" value="{{$user->name}}">
                                    </div>
                                    <div class="form-group" >
                                        <label>Count USD:</label>
                                        <input type="text" name="count" class="form-control" placeholder="" >
                                    </div>
                                    <div class="text-center">
                                        <button >Send</button>
                                    </div>
                                </form>
                            </div>


                        </div>

                </div>
            </div>
        </div>
    </div>
@endif
@if(isset($orders))
    @foreach($orders as $order)
        <div class="modal fade bs-example-modal-sm orderModal" tabindex="-1" role="dialog" id="orderModal-{{$order->id}}">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-order">
                    <div class="head">
                        <div class="title"><b>{{$order->info}}</b> details</div>
                        @if(!in_array($order->status, \App\Orders::$no_editable))
                            <button class="edit-order"><i class="fa fa-pencil"></i> EDIT</button>
                        @endif
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="container">
                        <form action="/order/{{$order->id}}/update" method="post" id="save-order-{{$order->id}}" enctype="multipart/form-data"
                              data-form-update-order="{{$order->id}}">

                            <div class="row">

                            <div class="info">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control" value="{{$order->name}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input type="email" name="email" class="form-control" value="{{$order->email}}" disabled required="true">
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" name="phone" class="form-control" value="{{$order->phone}}" disabled required="true">
                                </div>
                                <div class="form-group">
                                    <label>size of move</label>
                                    <select name="size" class="form-control" disabled>
                                        <option value="">Size of move:</option>
                                        <option value="Studio" @if($order->size == 'Studio') selected @endif>Studio</option>
                                        <option value="1 bedroom (Small)" @if($order->size == '1 bedroom (Small)') selected @endif>1 bedroom (Small)</option>
                                        <option value="1 bedroom (Large)" @if($order->size == '1 bedroom (Large)') selected @endif>1 bedroom (Large)</option>
                                        <option value="2 bedroom" @if($order->size == '2 bedroom') selected @endif>2 bedroom</option>
                                        <option value="3 bedroom" @if($order->size == '3 bedroom') selected @endif>3 bedroom</option>
                                        <option value="4+ bedroom" @if($order->size == '4+ bedroom') selected @endif>4+ bedroom</option>
                                        <option value="Commercial" @if($order->size == 'Commercial') selected @endif>Commercial</option>
                                        <option value="Storage" @if($order->size == 'Storage') selected @endif>Storage</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Moving from</label>
                                    <input type="text" name="from" class="form-control" value="{{$order->from}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Moving to</label>
                                    <input type="text" name="to" class="form-control" value="{{$order->to}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Moving date</label>
                                    <input type="text" name="date" class="form-control calend" value="{{($order->date) ? Carbon\Carbon::parse($order->date)->format('m/d/Y') : ''}}" disabled>
                                </div>
                                @if($order->info == 'On site estimate')
                                    <div class="form-group">
                                        <label>Moving time</label>
                                        <input type="text" name="time" class="form-control" data-time-format="h:i a" value="{{$order->time}}" disabled>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label style="margin-bottom: 7px">Notice</label>
                                    <textarea type="text" name="address" class="form-control" disabled>{{$order->address}}</textarea>
                                </div>
                                @if($order->file)
                                    <div class="form-group no-border">
                                        <label>inventory list</label>
                                        <a href="{{$order->file}}" target="_blank" class="download">
                                            <img src="/images/front/download.png"> <span class="file-name">Download.{{explode('.', $order->file)[1]}}</span>
                                        </a>
                                        @if($order->file && !$order->status)
                                            <label class="label-file" for="upload-file-{{$order->id}}">
                                                Replace file: <span> word, excel, pdf, jpg, etc</span>
                                            </label>
                                        @endif

                                    </div>
                                @endif
                                @if(!$order->file && !$order->status)
                                    <div class="form-group no-border no-file">
                                        <label>inventory list</label>
                                        <a href="{{$order->file}}" target="_blank" class="download">
                                            <img src="/images/front/download.png">
                                            <label class="label-file file-name" for="upload-file-{{$order->id}}">
                                                Upload <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Acceptable formats: word, excel, pdf, jpg, etc"></i>
                                            </label>
                                        </a>
                                    </div>
                                @endif
                                <input class="order-file" type="file" name="file" id="upload-file-{{$order->id}}" data-file-id="{{$order->id}}" style="display: none">

                            </div>
                                <div class="result">
                                    <div class="form-group">
                                        <label>job id</label>
                                        <p class="form-control">{{$order->id}}</p>
                                    </div>
                                    <div class="form-group">
                                        <label>status</label>
                                        <p class="form-control">{{\App\Orders::$status[$order->status]}}</p>
                                    </div>
                                    <div class="form-group">
                                        <label>JOB DURATION
                                        </label>
                                        @if($order->work)
                                            <p class="form-control">{{$order->work}} hours</p>
                                        @else
                                            <p class="form-control">-----------------</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>CUSTOMER PAID</label>
                                        @if($order->amount)
                                            <p class="form-control">${{$order->amount}}</p>
                                        @else
                                            <p class="form-control">-----------------</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>CUSTOMER SAVED</label>
                                        @if($order->customer_saved)
                                            <p class="form-control">${{$order->customer_saved}}</p>
                                        @else
                                            <p class="form-control">-----------------</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>MY COMMISION</label>
                                        @if($order->commission)
                                            <p class="form-control">${{$order->balance}} ({{$order->commission}}%)</p>
                                        @else
                                            <p class="form-control">-----------------</p>
                                        @endif
                                    </div>
                                    @if($order->check)
                                        <div class="form-group">
                                            <label>contract</label>
                                            <a href="{{$order->check}}" target="_blank" class="download">
                                                <img src="/images/front/download.png"> <span>Download.{{explode('.', $order->check)[1]}}</span>
                                            </a>
                                        </div>
                                    @endif
                                    <div class="form-group no-border">
                                        <label style="margin-bottom: 7px">LOYAL MOVING NOTES</label>
                                        @if($order->comment)
                                            <p class="form-control">{{$order->comment}}</p>
                                        @else
                                            <p class="form-control">-----------------</p>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="text-center">
                                    @if($order->status == 2)
                                        <a class="button" href="/order/{{$order->id}}/restore">Restore</a>
                                    @endif
                                    <button style="display: none" class="save-order-{{$order->id}}" data-form-id="save-order-{{$order->id}}">Save</button>
                                    <button data-dismiss="modal" aria-label="Close">Cancel</button>
                                    @if($order->status < 2)
                                        <a class="button" href="/order/{{$order->id}}/archive">Archive</a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
<div class="modal fade bs-example-modal-sm orderModal" tabindex="-1" role="dialog" id="orderModal-0">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-order">
<div class="head">
                <div class="title"><b>Quote</b> details</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="container">
                <form>

                    <div class="row">

                        <div class="info">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="John Malkovich" disabled required="true">
                            </div>
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="email" name="email" class="form-control" value="john@malkovich.com" disabled required="true">
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" name="phone" class="form-control" value="+1 (818) 000-0000" disabled required="true">
                            </div>
                            <div class="form-group">
                                <label>Size of move</label>
                                <select name="size" required="true" class="form-control" disabled>
                                    <option value="">Size of move:</option>
                                    <option value="Studio">Studio</option>
                                    <option value="1 bedroom (Small)" >1 bedroom (Small)</option>
                                    <option value="1 bedroom (Large)" >1 bedroom (Large)</option>
                                    <option value="2 bedroom" >2 bedroom</option>
                                    <option value="3 bedroom" >3 bedroom</option>
                                    <option value="4+ bedroom" selected>4+ bedroom</option>
                                    <option value="Commercial">Commercial</option>
                                    <option value="Storage">Storage</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Moving from</label>
                                <input type="text" name="from" class="form-control" value="Los Angeles, CA, United States" disabled required="true">
                            </div>
                            <div class="form-group">
                                <label>Moving to</label>
                                <input type="text" name="to" class="form-control" value="Las Vegas, NV, United States" disabled required="true">
                            </div>
                            <div class="form-group">
                                <label>Moving date</label>
                                <input type="text" name="date" class="form-control calend" value="{{\Carbon\Carbon::now()->format('m.d.Y')}}" disabled required="true">
                            </div>
                                <div class="form-group">
                                    <label>Moving time</label>
                                    <input type="text" name="time" class="form-control" data-time-format="h:i a" value="8AM" disabled>
                                </div>
                            <div class="form-group">
                                <label style="margin-bottom: 7px">Notes</label>
                                <textarea type="text" name="address" class="form-control" disabled>Overnight delivery, No Elevator</textarea>
                            </div>
                                <div class="form-group no-border">
                                    <label>inventory list</label>
                                    <a href="#" target="_blank" class="download">
                                        <img src="/images/front/download.png"> <span class="file-name">Download.PDF</span>
                                    </a>

                                </div>
                        </div>
                        <div class="result">
                            <div class="form-group">
                                <label>job id</label>
                                <p class="form-control">0</p>
                            </div>
                            <div class="form-group">
                                <label>status</label>
                                <p class="form-control">Complete</p>
                            </div>
                            <div class="form-group">
                                <label>job duration</label>
                                <p class="form-control">15 hours</p>
                            </div>                            
                            <div class="form-group">
                                <label>customer paid</label>
                                <p class="form-control">$2500</p>
                            </div>
                            <div class="form-group">
                                <label>customer saved</label>
                                <p class="form-control">$115</p>
                            </div>                            
                            <div class="form-group">
                                <label>My commision</label>
                                <p class="form-control">$250 (10%)</p>
                            </div>
                                <div class="form-group">
                                    <label>contract</label>
                                    <a href="#" target="_blank" class="download">
                                        <img src="/images/front/download.png"> <span>Download.PDF</span>
                                    </a>
                                </div>
                            <div class="form-group no-border">
                                <label style="margin-bottom: 7px">Loyal Moving Notes</label>
                                <p class="form-control">Good Move)</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="text-center">
                            <button data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm modal-sign" tabindex="-1" role="dialog" id="resetModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h3>Forgot Password</h3>
            <p class="info">Please enter your email address and we'll send you an email with a reset password link.</p>
            <form action="/reset/password" method="post" class="form-inline" data-form-reset-password="true">
                <div class="form-group">
                    <img src="/images/front/email.png" alt="">
                    <input type="email" name="reset_email"  class="form-control" placeholder="your@email.com" required="true">
                </div>
                <button>Send</button>
            </form>
            <div class="modal-footer">
                <p>Don't have an account? <a href="#" data-toggle="modal" data-target="#signUpModal">Create one now.</a></p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm modal-sign" tabindex="-1" role="dialog" id="signInModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h3>sign in</h3>
            <form action="/api/auth" method="post" data-form-auth="true">
                <div class="form-group">
                    <img src="/images/front/email.png" alt="">
                    <input type="email" name="email"  class="form-control" placeholder="your@email.com" required="true">
                </div>

                <div class="form-group">
                    <img src="/images/front/key-green.png" alt="">
                    <input type="password" name="password"  class="form-control" placeholder="Password" required="true">
                </div>
                <button>sign in</button>

            </form>
            <a href="#" data-toggle="modal" data-target="#resetModal" class="forgot">Forgot password?</a>
            <div class="modal-footer">
                <p>Don't have an account? <a href="#" data-toggle="modal" data-target="#signUpModal">Create one now.</a></p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm modal-sign" tabindex="-1" role="dialog" id="signUpModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h3>sign up</h3>
            <p class="info">Please enter your email address and we'll send you an confirmation email with password</p>
            <form action="/api/registration"  method="post" data-form-sing>
                <div class="form-group">
                    <img src="/images/front/email.png" alt="">
                    <input type="email" name="email"  class="form-control" placeholder="your@email.com" required="true">
                </div>
                <button>send</button>
            </form>
            <div class="modal-footer">
                <p>Have an account? <a href="#" data-toggle="modal" data-target="#signInModal">Sign In.</a></p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="loaderModal" style="z-index: 2000">
    <div class="loader">
        <div class="ball-scale-multiple">
            <div></div><div></div><div></div>
        </div>
    </div>
</div>
@if(Auth::check())
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="PromoModal">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h3>Send the Promo Code to Your Client</h3>
            <form action="/api/promo" method="post" data-form-promo>
                <div class="form-group">
                    <input class="styled-checkbox" id="type_send1" name="type_send[]" type="checkbox" value="1" required="true">
                    <label for="type_send1" class="info-label"></label>
                    <input type="email" name="email"  class="form-control" placeholder="Email:">
                </div>
                <div class="form-group">
                    <input class="styled-checkbox" id="type_send2" name="type_send[]" type="checkbox" value="2" required="true">
                    <label for="type_send2" class="info-label"></label>
                    <input type="text" name="phone"  class="form-control" placeholder="Phone:">
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="text" id="" cols="30" rows="10" required="true">Hi

This is {{$user->name}}, please use this {{$user->promo}} Promo code to get discounted price for your  upcoming move.
To apply the code click www.loyalmoving.com</textarea>
                </div>
                <div class="form-group text-center">
                    <button>send</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endif