<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>

<head>

    <title></title>

    <meta charset=utf-8>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><style type="text/css">
        @font-face {
            font-family: 'Raleway'; font-style: normal; font-weight: 400; src: local('Raleway'), local('Raleway-Regular'), url('https://fonts.gstatic.com/s/raleway/v11/bIcY3_3JNqUVRAQQRNVteQ.ttf') format('truetype');
        }
        @font-face {
            font-family: 'Raleway'; font-style: normal; font-weight: 500; src: local('Raleway Medium'), local('Raleway-Medium'), url('https://fonts.gstatic.com/s/raleway/v11/CcKI4k9un7TZVWzRVT-T8y3USBnSvpkopQaUR-2r7iU.ttf') format('truetype');
        }
        @font-face {
            font-family: 'Raleway'; font-style: normal; font-weight: 600; src: local('Raleway SemiBold'), local('Raleway-SemiBold'), url('https://fonts.gstatic.com/s/raleway/v11/xkvoNo9fC8O2RDydKj12by3USBnSvpkopQaUR-2r7iU.ttf') format('truetype');
        }
        @font-face {
            font-family: 'Raleway'; font-style: normal; font-weight: 700; src: local('Raleway Bold'), local('Raleway-Bold'), url('https://fonts.gstatic.com/s/raleway/v11/JbtMzqLaYbbbCL9X6EvaIy3USBnSvpkopQaUR-2r7iU.ttf') format('truetype');
        }
        @font-face {
            font-family: 'Raleway'; font-style: normal; font-weight: 800; src: local('Raleway ExtraBold'), local('Raleway-ExtraBold'), url('https://fonts.gstatic.com/s/raleway/v11/1ImRNPx4870-D9a1EBUdPC3USBnSvpkopQaUR-2r7iU.ttf') format('truetype');
        }
        @font-face {
            font-family: 'Raleway'; font-style: normal; font-weight: 900; src: local('Raleway Black'), local('Raleway-Black'), url('https://fonts.gstatic.com/s/raleway/v11/PKCRbVvRfd5n7BTjtGiFZC3USBnSvpkopQaUR-2r7iU.ttf') format('truetype');
        }
        @font-face {
            font-family: 'Roboto'; font-style: normal; font-weight: 300; src: local('Roboto Light'), local('Roboto-Light'), url('https://fonts.gstatic.com/s/roboto/v15/Hgo13k-tfSpn0qi1SFdUfaCWcynf_cDxXwCLxiixG1c.ttf') format('truetype');
        }
        @font-face {
            font-family: 'Roboto'; font-style: normal; font-weight: 400; src: local('Roboto'), local('Roboto-Regular'), url('https://fonts.gstatic.com/s/roboto/v15/zN7GBFwfMP4uA6AR0HCoLQ.ttf') format('truetype');
        }
        @font-face {
            font-family: 'Roboto'; font-style: normal; font-weight: 700; src: local('Roboto Bold'), local('Roboto-Bold'), url('https://fonts.gstatic.com/s/roboto/v15/d-6IYplOFocCacKzxwXSOKCWcynf_cDxXwCLxiixG1c.ttf') format('truetype');
        }
        @font-face {
            font-family: 'Roboto'; font-style: normal; font-weight: 900; src: local('Roboto Black'), local('Roboto-Black'), url('https://fonts.gstatic.com/s/roboto/v15/mnpfi9pxYH-Go5UiibESIqCWcynf_cDxXwCLxiixG1c.ttf') format('truetype');
        }
        @font-face {
            font-family: 'Roboto'; font-style: italic; font-weight: 400; src: local('Roboto Italic'), local('Roboto-Italic'), url('https://fonts.gstatic.com/s/roboto/v15/W4wDsBUluyw0tK3tykhXEfesZW2xOQ-xsNqO47m55DA.ttf') format('truetype');
        }
        .ReadMsgBody {
            width: 100%;
        }
        .ExternalClass {
            width: 100%;
        }
        .ExternalClass {
            line-height: 100%;
        }
        img {
            display: block;
        }
        body {
            padding: 0; width: 100% !important;
        }
        body {
            -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;
        }
        .ReadMsgBody {
            width: 100%;
        }
        .ExternalClass {
            width: 100%;
        }
        body {
            background-color: #ffffff; font-family: Arial, Helvetica,; font-size: 14px; color: #393939; margin: 0 auto;
        }
        @media (max-width: 768px) and (min-width: 461px) {
            body {margin: 0 auto !important; width: 600px !important;
            }
            img {height: auto !important; max-width: 100% !important;
            }
            .wrapper_table {width: 600px !important; margin: 0 auto; text-align: center; background-size: 100% 100% !important;
            }
            .content {width: 600px !important;
            }
            .content img {max-width: 600px !important;
            }
            .padding {width: 20px !important;
            }
            .content_row {width: 560px !important;
            }
            .content_row img {height: auto !important; max-width: 560px !important;
            }
            .content_row_inner {width: 520px !important;
            }
            .col_md_6 {width: 85px !important; display: inline-block;
            }
            .col_md_5 {width: 104px !important; display: inline-block;
            }
            .col_md_4 {width: 132.5px !important; display: inline-block;
            }
            .col_md_3 {width: 180px !important; display: inline-block;
            }
            .col_md_2 {width: 275px !important; display: inline-block;
            }
            .col_md_23 {width: 367px !important; display: inline-block;
            }
            .col_md_13 {width: 183px !important; display: inline-block;
            }
            .col_md_6_fullwidth {width: 100px !important; display: table-cell;
            }
            .col_md_5_fullwidth {width: 120px !important; display: table-cell;
            }
            .col_md_4_fullwidth {width: 150px !important; display: table-cell;
            }
            .col_md_3_fullwidth {width: 200px !important; display: table-cell;
            }
            .col_md_2_fullwidth {width: 300px !important; display: table-cell;
            }
            .col_md_23_fullwidth {width: 400px !important; display: table-cell;
            }
            .col_md_13_fullwidth {width: 200px !important; display: table-cell;
            }
            .col_md_6_fullwidth img {max-width: 100px !important; height: auto !important;
            }
            .col_md_5_fullwidth img {max-width: 120px !important; height: auto !important;
            }
            .col_md_4_fullwidth img {max-width: 150px !important; height: auto !important;
            }
            .col_md_3_fullwidth img {max-width: 200px !important; height: auto !important;
            }
            .col_md_2_fullwidth img {max-width: 300px !important; height: auto !important;
            }
            .col_md_13_fullwidth img {max-width: 200px !important; height: auto !important;
            }
            .col_md_23_fullwidth img {max-width: 400px !important; height: auto !important;
            }
            .col_md_23 img {max-width: 100% !important; display: block; height: auto !important;
            }
            .col_md_13 img {max-width: 100% !important; display: block; height: auto !important;
            }
            .col_md_2 img {max-width: 100% !important; display: block; width: auto !important; height: auto !important;
            }
            .col_md_3 img {max-width: 100% !important; display: block; width: auto !important; height: auto !important;
            }
            .col_md_6_fullwidth_img {width: 100px !important; height: auto !important;
            }
            .col_md_5_fullwidth_img {width: 120px !important; height: auto !important;
            }
            .col_md_4_fullwidth_img {width: 150px !important; height: auto !important;
            }
            .col_md_3_fullwidth_img {width: 200px !important; height: auto !important;
            }
            .col_md_2_fullwidth_img {width: 300px !important; height: auto !important;
            }
            .col_md_23_fullwidth_img {width: 400px !important; height: auto !important;
            }
            .col_md_13_fullwidth_img {width: 200px !important; height: auto !important;
            }
            .col_md_2_nomargins {width: 280px !important; display: inline-block !important; float: left;
            }
            .col_md_2_nomargins img {max-width: 280px !important; height: auto !important;
            }
            .col_md_3_nomargins {width: 186px !important; display: inline-block !important; float: left;
            }
            .col_md_3_nomargins img {max-width: 186px !important; height: auto !important;
            }
            .col_md_4_nomargins {width: 140px !important; display: inline-block !important; float: left;
            }
            .col_md_4_nomargins img {max-width: 140px !important; height: auto !important;
            }
            .col_md_5_nomargins {width: 112px !important; display: inline-block !important; float: left;
            }
            .col_md_6_nomargins {width: 93px !important; display: inline-block !important; float: left;
            }
            .col_md_13_nomargins {width: 186px !important; display: inline-block !important; float: left;
            }
            .col_md_13_nomargins img {max-width: 186px !important; height: auto !important;
            }
            .col_md_23_nomargins {width: 374px !important; display: inline-block !important; float: left;
            }
            .col_md_23_nomargins img {max-width: 374px !important; height: auto !important;
            }
            .col_md_2_inner {width: 255px !important; display: inline-block;
            }
            .col_md_2_inner img {max-width: 255px !important; height: auto !important;
            }
            .col_md_3_inner {width: 166px !important; display: inline-block;
            }
            .col_md_3_inner img {max-width: 166px !important; height: auto !important;
            }
            .col_md_4_inner {width: 122px !important; display: inline-block;
            }
            .col_md_4_inner img {max-width: 122px !important; height: auto !important;
            }
            .col_md_5_inner {width: 96px !important; display: inline-block;
            }
            .col_md_5_inner img {max-width: 96px !important; height: auto !important;
            }
            .col_md_6_inner {width: 78px !important; display: inline-block;
            }
            .col_md_6_inner img {max-width: 78px !important; height: auto !important;
            }
            .col_md_23_inner {width: 340px !important; display: inline-block;
            }
            .col_md_23_inner img {max-width: 340px !important; height: auto !important;
            }
            .col_md_13_inner {width: 170px !important; display: inline-block;
            }
            .col_md_2_inner img {max-width: 170px !important; height: auto !important;
            }
            .col_md_6_space {width: 10px;
            }
            .col_md_5_space {width: 10px;
            }
            .col_md_4_space {width: 10px;
            }
            .col_md_3_space {width: 10px;
            }
            .col_md_2_space {width: 10px;
            }
            .col_md_23_space {width: 10px;
            }
            .col_sm_6_space {width: 10px;
            }
            .col_sm_5_space {width: 10px;
            }
            .col_sm_4_space {width: 10px;
            }
            .col_sm_3_space {width: 10px;
            }
            .col_sm_2_space {width: 10px;
            }
            .col_sm_3 {width: 180px !important; display: inline-block;
            }
            .col_sm_2 {width: 275px !important; display: inline-block;
            }
            .notablet {display: none !important;
            }
            .noresponsive {display: none !important;
            }
            .font10 {font-size: 15px !important;
            }
            .font15 {font-size: 20px !important;
            }
            .font20 {font-size: 25px !important;
            }
            .font25 {font-size: 30px !important;
            }
            .font30 {font-size: 35px !important;
            }
            .font35 {font-size: 40px !important;
            }
            .font40 {font-size: 45px !important;
            }
            .font45 {font-size: 50px !important;
            }
            .height10 {height: 15px !important; display: block !important;
            }
            .height15 {height: 20px !important; display: block !important;
            }
            .height20 {height: 25px !important; display: block !important;
            }
            .height25 {height: 30px !important; display: block !important;
            }
            .height30 {height: 35px !important; display: block !important;
            }
            .height35 {height: 40px !important; display: block !important;
            }
            .height40 {height: 45px !important; display: block !important;
            }
            .noborder {border: 0 !important;
            }
            .nomargin {margin: 0 !important;
            }
            .nomargin_tablet {margin: 0 !important;
            }
            .tablet_margin_top_10 {margin-top: 10px !important;
            }
            .tablet_margin_bottom_10 {margin-bottom: 10px !important;
            }
            .tablet_margin_top_20 {margin-top: 20px !important;
            }
            .tablet_margin_bottom_20 {margin-bottom: 20px !important;
            }
            .tablet_margin_top_30 {margin-top: 30px !important;
            }
            .tablet_margin_bottom_30 {margin-bottom: 30px !important;
            }
            .tablet_margin_top_10 {margin-top: 10px !important;
            }
            .tablet_margin_bottom_10 {margin-bottom: 10px !important;
            }
            .tablet_margin_top_20 {margin-top: 20px !important;
            }
            .tablet_margin_bottom_20 {margin-bottom: 20px !important;
            }
            .tablet_margin_top_30 {margin-top: 30px !important;
            }
            .tablet_margin_bottom_30 {margin-bottom: 30px !important;
            }
            .no_desktop {display: block !important; visibility: visible;
            }
            .stack_responsive {width: 100% !important; display: inline-block;
            }
            .stacked {display: block; clear: both; float: none;
            }
            .text_center_responsive {text-align: center !important;
            }
            .align_right {text-align: right !important;
            }
            .align_left {text-align: left !important;
            }
            .align_right_tablet {text-align: right !important;
            }
            .align_left_tablet {text-align: left !important;
            }
            .align_right_responsive {text-align: right !important;
            }
            .align_left_responsive {text-align: left !important;
            }
            .tablet_padding_top_10 {padding-top: 10px !important;
            }
            .align_right {text-align: right !important;
            }
            .align_left {text-align: left !important;
            }
            .image img {height: auto !important; max-width: 100% !important;
            }
            .responsive_centered_table {margin: 0 auto !important; text-align: center !important;
            }
            .display_table_cell_tablet {display: table-cell !important;
            }
            .header_group_tablet {display: table-header-group !important;
            }
            .footer_group_tablet {display: table-footer-group !important;
            }
            .header_group_responsive {display: table-header-group !important;
            }
            .footer_group_responsive {display: table-footer-group !important;
            }
            .price_tab_desc {width: 265px !important; display: block !important;
            }
            .price_tab_desc_first {width: 230px !important; display: block !important;
            }
            .price_table_qty {width: 35px !important; display: block !important;
            }
            .price_table_size {width: 35px !important; display: block !important;
            }
            .price_table_color {width: 55px !important; display: block !important;
            }
            .price_table_amount {width: 60px !important; display: block !important;
            }
            .price_tab_desc_smaller {width: 236px !important; display: block !important;
            }
            .no_lineheight {line-height: 0 !important;
            }
            .font_100m {font-size: 100px !important;
            }
            #no_bg {background-image: none !important;
            }
            .image_bottom img {width: 100% !important; display: block !important;
            }
            #no_bgresponsive {background-image: none !important;
            }
            .width200 {width: 200px !important;
            }
            .tablet_padding {width: 50px !important;
            }
        }
        @media (max-width: 461px) {
            body.responsive {margin: 0 auto !important; width: 320px !important;
            }
            img {height: auto !important; max-width: 100% !important;
            }
            table.wrapper_table {width: 320px !important; background-size: 100% 100% !important;
            }
            table.wrapper_table .content {width: 320px !important;
            }
            table.wrapper_table .content img {max-width: 100% !important;
            }
            table.wrapper_table .padding {width: 10px !important;
            }
            table.wrapper_table .content_row {width: 300px !important;
            }
            table.wrapper_table .content_row img {max-width: 300px !important; height: auto !important;
            }
            table.wrapper_table .content_row_inner {width: 280px !important;
            }
            table.mobile_centered_table {margin: 0 auto !important; text-align: center !important;
            }
            td.mobile_centered_table {margin: 0 auto !important; text-align: center !important;
            }
            table.wrapper_table .col_md_6 {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_5 {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_4 {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_3 {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_2 {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_6_inner {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_5_inner {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_4_inner {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_3_inner {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_2_inner {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_23 {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_13 {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_23_inner {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_13_inner {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_6_nomargins {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_5_nomargins {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_4_nomargins {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_3_nomargins {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_2_nomargins {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_23_nomargins {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_13_nomargins {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_6_mobile {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_5_mobile {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_4_mobile {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_3_mobile {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_2_mobile {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_23_fullwidth {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_13_fullwidth {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_6_fullwidth {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_5_fullwidth {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_4_fullwidth {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_3_fullwidth {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_2_fullwidth {width: 100% !important; display: block !important;
            }
            table.wrapper_table .col_md_6_space {display: none !important; margin-bottom: 20px;
            }
            table.wrapper_table .col_md_5_space {display: none !important; margin-bottom: 20px;
            }
            table.wrapper_table .col_md_4_space {display: none !important; margin-bottom: 20px;
            }
            table.wrapper_table .col_md_3_space {display: none !important; margin-bottom: 20px;
            }
            table.wrapper_table .col_md_2_space {display: none !important; margin-bottom: 20px;
            }
            table.wrapper_table .col_md_23_space {display: none !important; margin-bottom: 20px;
            }
            table.wrapper_table .col_sm_3_space {margin-bottom: 20px; width: 10px; display: block;
            }
            table.wrapper_table .col_sm_2_space {margin-bottom: 20px; width: 10px; display: block;
            }
            table.wrapper_table .col_md_6 img {max-width: 100%; display: block; width: auto !important; height: auto !important;
            }
            table.wrapper_table .col_md_5 img {max-width: 100%; display: block; width: auto !important; height: auto !important;
            }
            table.wrapper_table .col_md_4 img {max-width: 100%; display: block; width: auto !important; height: auto !important;
            }
            table.wrapper_table .col_md_3 img {max-width: 100%; display: block; width: auto !important; height: auto !important;
            }
            table.wrapper_table .col_md_2 img {max-width: 100%; display: block; width: auto !important; height: auto !important;
            }
            table.wrapper_table .col_md_1 img {max-width: 100%; display: block; width: auto !important; height: auto !important;
            }
            table.wrapper_table .col_md_23 img {display: block; height: auto !important;
            }
            table.wrapper_table .col_sm_3 {width: 93.3px !important;
            }
            table.wrapper_table .col_sm_2 {width: 145px !important;
            }
            table.wrapper_table .font10 {font-size: 10px !important;
            }
            table.wrapper_table .font15 {font-size: 15px !important;
            }
            table.wrapper_table .font20 {font-size: 20px !important;
            }
            table.wrapper_table .font25 {font-size: 25px !important;
            }
            table.wrapper_table .font30 {font-size: 30px !important;
            }
            table.wrapper_table .font35 {font-size: 35px !important;
            }
            table.wrapper_table .font40 {font-size: 40px !important;
            }
            table.wrapper_table .mob_font10 {font-size: 10px !important;
            }
            table.wrapper_table .mob_font15 {font-size: 15px !important;
            }
            table.wrapper_table .mob_font20 {font-size: 20px !important;
            }
            table.wrapper_table .mob_font25 {font-size: 25px !important;
            }
            table.wrapper_table .mob_font30 {font-size: 30px !important;
            }
            table.wrapper_table .mob_font40 {font-size: 40px !important;
            }
            table.wrapper_table .mob_font100 {font-size: 100px !important;
            }
            table.wrapper_table .height10 {height: 10px !important; display: block !important;
            }
            table.wrapper_table .height15 {height: 15px !important; display: block !important;
            }
            table.wrapper_table .height20 {height: 20px !important; display: block !important;
            }
            table.wrapper_table .height25 {height: 25px !important; display: block !important;
            }
            table.wrapper_table .height30 {height: 30px !important; display: block !important;
            }
            table.wrapper_table .height35 {height: 35px !important; display: block !important;
            }
            table.wrapper_table .height40 {height: 40px !important; display: block !important;
            }
            table.wrapper_table .mobile_height10 {height: 10px !important; display: block !important;
            }
            table.wrapper_table .mobile_height15 {height: 15px !important; display: block !important;
            }
            table.wrapper_table .mobile_height20 {height: 20px !important; display: block !important;
            }
            table.wrapper_table .mobile_height25 {height: 25px !important; display: block !important;
            }
            table.wrapper_table .mobile_height30 {height: 30px !important; display: block !important;
            }
            table.wrapper_table .mobile_height35 {height: 35px !important; display: block !important;
            }
            table.wrapper_table .mobile_height40 {height: 40px !important; display: block !important;
            }
            table.wrapper_table .noresponsive {display: none !important;
            }
            table.wrapper_table .nomobile {display: none !important;
            }
            table.wrapper_table .text_center {text-align: center !important;
            }
            table.wrapper_table .aligncenter {text-align: center !important; margin: 0 auto !important;
            }
            table.wrapper_table .stacked_fullwidth {width: 100%; display: block !important;
            }
            table.wrapper_table .stacked_fullwidth_centered {max-width: 100% !important; display: block !important; margin: 0 auto !important;
            }
            table.wrapper_table .stacked {display: block; clear: both; float: none;
            }
            table.wrapper_table .noborder {border: 0 !important;
            }
            table.wrapper_table .noborder_mobile {border: 0 !important;
            }
            table.wrapper_table .nomargin {margin: 0 !important;
            }
            table.wrapper_table .nomargin_mobile {margin: 0 !important;
            }
            table.wrapper_table .margin_top_10 {margin-top: 10px !important;
            }
            table.wrapper_table .mobile_margin_top_10 {margin-top: 10px !important;
            }
            table.wrapper_table .margin_bottom_10 {margin-bottom: 10px !important;
            }
            table.wrapper_table .mobile_margin_bottom_10 {margin-bottom: 10px !important;
            }
            table.wrapper_table .margin_top_20 {margin-top: 20px !important;
            }
            table.wrapper_table .mobile_margin_top_20 {margin-top: 20px !important;
            }
            table.wrapper_table .margin_bottom_20 {margin-bottom: 20px !important;
            }
            table.wrapper_table .mobile_margin_bottom_20 {margin-bottom: 20px !important;
            }
            table.wrapper_table .margin_top_30 {margin-top: 30px !important;
            }
            table.wrapper_table .mobile_margin_top_30 {margin-top: 30px !important;
            }
            table.wrapper_table .margin_bottom_30 {margin-bottom: 30px !important;
            }
            table.wrapper_table .mobile_margin_bottom_30 {margin-bottom: 30px !important;
            }
            table.wrapper_table .stack_responsive {width: 100%; display: block !important;
            }
            table.wrapper_table .header_group {display: table-header-group !important; margin-bottom: 20px !important;
            }
            table.wrapper_table .footer_group {display: table-footer-group !important;
            }
            table.wrapper_table .header_group_responsive {display: table-header-group !important; margin-bottom: 20px !important;
            }
            table.wrapper_table .footer_group_responsive {display: table-footer-group !important;
            }
            table.wrapper_table .text_center_responsive {text-align: center;
            }
            table.wrapper_table .text_center_responsive {text-align: center;
            }
            table.wrapper_table .align_right {text-align: right !important;
            }
            table.wrapper_table .align_left {text-align: left !important;
            }
            table.wrapper_table .align_right_mobile {text-align: right !important;
            }
            table.wrapper_table .align_left_mobile {text-align: left !important;
            }
            table.wrapper_table .align_right_responsive {text-align: right !important;
            }
            table.wrapper_table .align_left_responsive {text-align: left !important;
            }
            table.wrapper_table .padding_top_10 {padding-top: 10px !important;
            }
            table.wrapper_table .mobile_padding_top_10 {padding-top: 10px !important;
            }
            table.wrapper_table .responsive_centered_table {margin: 0 auto !important; text-align: center !important;
            }
            table#nobg_mobile {background-image: none !important;
            }
            table.wrapper_table .image img {display: block; width: 100% !important; height: auto !important;
            }
            table.wrapper_table .desktop_hide {max-height: none !important; font-size: 12px !important; display: block !important;
            }
            table.wrapper_table .desktop_hide_img {max-height: none !important; width: auto !important; display: block !important; visibility: visible !important;
            }
            table.wrapper_table .col_md_2_mobile_text {width: 200px !important;
            }
            table.wrapper_table .centered_image {margin: 0 auto !important;
            }
            table#no_bgresponsive {background-image: none !important;
            }
            table.wrapper_table .width200 {width: 10px !important;
            }
            table.wrapper_table .no_lineheight {line-height: 0 !important;
            }
            table.wrapper_table .mobile_padding {width: 10px !important;
            }
        }
    </style>
</head>

<body class="responsive" style="width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-family: Arial, Helvetica,; font-size: 14px; color: #393939; background: #ffffff; margin: 0 auto; padding: 0;">
@yield('content')
</body>
</html>