<div class="create-order">
    <div class="customer-info">
        <div class="top">
            <i class="fa fa-info-circle"></i> CUSTOMER INFO
        </div>
        <div class="body">
            <form action="/profile/order" method="post" enctype="multipart/form-data" data-form-orders>
                <ul class="radio">
                    <li>{{Form::radio('info', 'Quote', true, ['required'] )}} <span>Quote</span></li>
                    <li>{{Form::radio('info', 'Reserve', old('info'), ['required'] )}} <span>Reserve</span></li>
                    <li>{{Form::radio('info', 'On site estimate', old('info'), ['required'] )}} <span>On site estimate</span></li>
                </ul>
                <div @if(stristr($_SERVER['REQUEST_URI'], 'profile')) data-step="1" @endif>
                    <div class="form-group">
                        {{Form::text('name', old('name'), ['class' => 'form-control', 'placeholder'=>'Name:'])}}
                    </div>
                    <div class="form-group">
                        {{Form::email('email', old('email'), ['class' => 'form-control', 'placeholder'=>'Email:', 'required'])}}
                    </div>
                    <div class="form-group">
                        <input class="styled-checkbox" id="copy" name="copy" type="checkbox" >
                        <label for="copy" class="info-label">Receive copy</label>
                    </div>
                    <div class="form-group">
                        {{Form::text('phone', old('phone'), ['class' => 'form-control', 'placeholder'=>'Phone number:', 'required'])}}
                    </div>
                    <div class="form-group">
                        <select name="size" class="form-control">
                            <option value="" selected="">Size of move:</option>
                            <option value="Studio">Studio</option>
                            <option value="1 bedroom (Small)">1 bedroom (Small)</option>
                            <option value="1 bedroom (Large)">1 bedroom (Large)</option>
                            <option value="2 bedroom">2 bedroom</option>
                            <option value="3 bedroom">3 bedroom</option>
                            <option value="4+ bedroom">4+ bedroom</option>
                            <option value="Commercial">Commercial</option>
                            <option value="Storage">Storage</option>
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::text('from', old('from'), ['class' => 'form-control geocomplete', 'placeholder'=>'Moving from:'])}}
                    </div>
                    <div class="form-group">
                        {{Form::text('to', old('to'), ['class' => 'form-control geocomplete', 'placeholder'=>'Moving to:'])}}
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 data-block" >
                            {{Form::text('date', old('date'), ['class' => 'form-control calend', 'data-placeholder'=> 'Moving date:','placeholder'=>'Moving date:'])}}
                        </div>
                        <div class="form-group col-xs-5 time-block" style="padding-left: 0">
                            {{Form::text('time', old('time'), ['class' => 'form-control','data-time-format'=>"h:i a",'placeholder'=>'Time:' ])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::textarea('address', old('address'), ['class' => 'form-control', 'placeholder'=>'Notes:',
                         'rows' => 5,'cols' => "30"])}}
                    </div>
                </div>

                <div class="inventory">
                    <label for="file" class="label-file file-name"><i class="fa fa-paperclip"></i> inventory list</label>
                    <input type="file" name="file" id="file" style="display: none" class="order-file">
                    <p>word, excel, pdf, jpg, etc files</p>
                </div>
                <div class="text-center">
                    <button>Send</button>
                </div>

            </form>

        </div>
    </div>
</div>
