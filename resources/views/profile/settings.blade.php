@extends('layouts.lk')

@section('content')

<section class="profile-settings">
    <h1 class="title">Settings</h1>
    <div class="container">
        <div class="row">
            <form action="/settings" method="post" class="form-horizontal" enctype="multipart/form-data" data-form-settings>
                    <div class="form-group">
                        <div id="image-wrapper">
                            <img src="@if($user->avatar){{$user->avatar}}@else /images/front/default_avatar.png @endif" alt="" class="avatar avatar-preview">
                            <input type="file" id="avatar" name="avatar">
                            <input type="hidden" id="avatar_crop" name="avatar_crop">
                        </div>

                            <div class="avatar-text">
                                <h3 class="name">{{$user->name}}</h3>
                                <p class="email">{{$user->email}}</p>
                                <label for="avatar" class="upload">Upload your profile picture</label>
                            </div>

                    </div>
                    <div class="form-group">
                        <label for="name">Your name:</label>
                        <input type="text" name="name" placeholder="" id="name" class="form-control @if ($errors->has('name')) error-input @endif" value="{{($user->name) ? $user->name : old('name')}}">

                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="text" name="email" placeholder="" id="email" class="form-control" disabled value="{{$user->email}}">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone number:</label>
                        <input type="text" name="phone" placeholder="+1 (___) ___-__-__" id="phone" class="form-control @if ($errors->has('phone')) error-input @endif" value="{{$user->phone ? $user->phone : old('phone')}}">
                    </div>
                    <div class="form-group">
                        <label id="specialist">Specialist:</label>
                        <div class="row specialist">
                            <div class="col-md-6">
                                <input type="radio" name="specialist" value="1" @if($user->specialist == 1) checked @endif> <span>Realtor</span> <br>
                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="specialist" value="2" @if($user->specialist == 2) checked @endif> <span>Building manager</span> <br>
                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="specialist" value="3" @if($user->specialist == 3) checked @endif> <span>Lawyer</span> <br>
                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="specialist" value="4" @if($user->specialist == 4) checked @endif> <span>Contractor</span> <br>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="other" placeholder="Other" id="other" class="form-control @if ($errors->has('other')) error-input @endif" value="{{$user->other ? $user->other : old('other') }}">
                    </div>
                    <div class="form-group">
                        <label for="address">Address:</label>
                        <input type="text" name="address" placeholder="Street: ex: 777 Main St. unit 77" id="address"
                               class="form-control @if ($errors->has('address')) error-input @endif"
                               value="{{$user->address ? $user->address : old('address')}}">
                    </div>
                    <div class="form-group">
                        <label for="address">City:</label>
                        <input type="text" name="city" placeholder="" id="address"
                               class="form-control"
                               value="{{$user->city}}">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="state">State/Probince/Region:</label>
                                <input type="text" name="state" placeholder="" id="state"
                                       class="form-control"
                                       value="{{$user->state}}">
                            </div>
                            <div class="col-md-6">
                                <label for="zip">ZIP/Postal code:</label>
                                <input type="text" name="zip" placeholder="" id="zip"
                                       class="form-control"
                                       value="{{$user->zip}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">New password:</label>
                        <input type="password" name="password" id="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Repeat  password:</label>
                        <input type="password" name="password_confirmation" placeholder="" id="password_confirmation"
                               class="form-control @if ($errors->has('password_confirmation')) error-input @endif">
                    </div>

                    <div class="form-group text-center">
                        <button class="upload-result">Confirm</button>
                    </div>
            </form>
        </div>
    </div>
</section>
<script>
    var inpElem = document.getElementById('avatar');
    var first = null;

    inpElem.addEventListener("change", function(e) {
        preview(this.files[0], '.avatar-preview');
    });

    function preview(file, selector) {
        if ( file.type.match(/image.*/) ) {
            var reader = new FileReader(), img;
            reader.addEventListener("load", function(event) {
                img = document.createElement('img');
                img.src = event.target.result;

                $(selector).attr('src', img.src);
                crop(first);

            });
            reader.readAsDataURL(file);
        }
    }

    function crop (reader) {
        var mc = $('.avatar-preview');
        mc.croppie({
            viewport: {
                width: 120,
                height: 120,
                type: 'circle'
            },
            boundary: {
                width: 130,
                height: 130
            }
        });

        $('.upload-result').on('click', function (ev) {
            ev.preventDefault();
            mc.croppie('result', {
                type: 'canvas',
                size: 'original'
            }).then(function (canvas) {
                $('#avatar_crop').val(canvas);
                setTimeout(function () {
                    $('[data-form-settings]').submit();
                },300);
            });
        });

        if(reader){
            var content;
            content =$(".croppie-container .croppie-container").detach();
            content.appendTo('#image-wrapper');
            $(".croppie-container").first().remove();
        }

        first =1;
    }
</script>
@endsection
