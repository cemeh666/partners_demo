@extends('layouts.lk')

@section('content')
    <style>
        body{
            min-width: 800px;
        }

        @media only screen and (max-width: 700px) {
            .create-order{
                width: 250px;
                float: right;
            }
            header .navigate-mobile {
                display: none;
            }
            header .navigate {
                display: inline-block;
            }
            header .auth {
                display: block;
            }
            header .container {
                padding-top: 15px;
            }
            .items{
                width: 400px;
                float: left;
            }
        }

        @media only screen and (max-width: 785px) {
            header .auth li {
                padding-left: 30px;
            }
            header .navigate li {
                padding-right: 30px;
            }
        }

        @media only screen and (max-width: 1100px) {
            section.profile-body .table-head .filter-name {
                width: 33%;
            }
            section.profile-body .table-head .filter-name input {
                width: 100%;
            }
            section.profile-body .table-head .filter-date {
                width: 211px;
            }
            section.profile-body .table-head .filter-status {
                width: 140px;
            }
        }
        @media only screen and (max-width: 780px) {
            section.profile-head .head .promo-wrapper {
                position: absolute;
                top: 28px;
                left: 40%;
            }
            section.profile-head {
                height: auto;
            }
        }
        @media only screen and (max-width: 860px) {
            section.profile-body .table-head .filter-name {
                width: 100%;
            }
            section.profile-body .table-head .filter-status {
                text-align: center;
                width: 285px;
                float: right;
            }
            section.profile-body .table-head .filter-date {
                text-align: center;
                width: 211px;
                float: none;
            }
        }
        @media only screen and (max-width: 470px) {
            section.profile-head .head img {
                height: 35px;
            }
            section.profile-head .head p {
                font-size: 18px;
                display: inline-block;
                line-height: 24px;
            }
            section.profile-head .head .delimiter {
                height: 55px;
                margin: 0 20px 0 10px;
            }
        }
        @media only screen and (max-width: 770px) {
            .items {
                width: 65%;
            }
        }
        @media only screen and (max-width: 350px) {
            section.profile-head .container {
                padding: 0 15px 0 15px;
            }
        }
    </style>
<section class="profile-body">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('layouts._order')
                <div class="items">
                    <div class="table-head">
                        <form action="/profile" method="get" class="profile-order">
                            <div class="filter-name">
                                <i class="fa fa-search search"></i>
                                <input type="text" name="search" placeholder="Name:" class="form-control" value="{{$search}}">
                            </div>
                            <div class="filter-date">
                                <button style="background: transparent"><i class="fa fa-2x fa-calendar-o"></i></button>
                                <input type="text" name="date_order" data-placeholder="filter with date" placeholder="filter with date"  class="calendar-range" id="date_order" value="{{$date}}">
                                @if($date)<button class="fa fa-close text-danger date-reset"></button>@endif
                            </div>
                            <div class="filter-status">
                                <i class="fa fa-2x fa-hourglass-half"></i> Status: <a href="#" id="status">
                                    <b>{{$status}}</b>
                                </a>
                                <div class="show-status">
                                    <ul>
                                        <li>
                                            <a href="#" data-order-status="-1">Show all</a>
                                        </li>
                                        @foreach(\App\Orders::$status as $key => $status)
                                            <li>
                                                <a href="#" data-order-status="{{$key}}">{{$status}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <input type="hidden" name="status" id="status-order" value="{{$status_id}}">

                        </form>
                    </div>
                    <div class="table-orders">
                        <a href="#" class="table-row" data-step="3" data-toggle="modal" data-target="#orderModal-0">
                            <div class="name col">
                                <span>John Malkovich</span>
                                <p class="date-mobile">{{\Carbon\Carbon::now()->format('m/d/Y')}}</p>
                            </div>
                            <div class="date col">{{\Carbon\Carbon::now()->format('m/d/Y')}}</div>
                            <div class="status col  complete ">Completed</div>
                            <div class="balance col">$250</div>
                        </a>
                        @foreach($orders as $order)
                            <a href="#" data-toggle="modal" data-target="#orderModal-{{$order->id}}" class="table-row">
                                <div class="name col">
                                    <span>{{$order->name}}</span>
                                    <p class="date-mobile">{{($order->date) ? Carbon\Carbon::parse($order->date)->format('m/d/Y') : ''}}</p>
                                </div>
                                <div class="date col">{{($order->date) ? Carbon\Carbon::parse($order->date)->format('m/d/Y') : ''}}</div>
                                <div class="status col @if($order->status == 1) complete @endif">{{\App\Orders::$status[$order->status]}}</div>
                                <div class="balance col">${{$order->balance}}</div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection
@section('script')
    <script>
    if(!getCookie('profile'))
        startTutorialProfile();

    </script>
@endsection