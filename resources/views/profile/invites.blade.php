@extends('layouts.lk')

@section('content')

<section class="profile-body">
    <div class="container invites">
        <div class="row">
            <div class="col-md-12">

                @include('layouts._order')

                <div class="items">
                    <div class="title">
                        List of invitations:
                    </div>
                    <div class="invite-block">
                        <div class="row">
                            <div class="code">
                                <p><strong>Referral code:</strong></p>
                                <a target="_blank" href="{{env('APP_URL')}}?invite={{$user->id}}">{{env('APP_URL')}}?invite={{$user->id}}</a>
                            </div>
                            <div class="send" data-step="1" >
                                <form action="/send/invite" method="post" class="form-inline">
                                    <input type="email" name="email" class="form-control" placeholder="Email:" required="true">

                                    <button>Send</button>

                                </form>
                            </div>
                        </div>

                    </div>
                    <div class="table-head">
                        <form action="/invites" method="get" class="profile-order">
                            <div class="filter-name">
                                <i class="fa fa-search search"></i>
                                <input type="text" name="search" placeholder="Email:" class="form-control" value="{{$search}}">
                            </div>
                            <div class="filter-date">
                                <button style="background: transparent"><i class="fa fa-2x fa-calendar-o"></i></button>
                                <input type="text" name="date" placeholder="filter with date" data-placeholder="filter with date" class="cal" id="date_order" value="{{$date}}">
                                @if($date)<button class="fa fa-close text-danger date-reset"></button>@endif

                            </div>
                            <div class="filter-status">
                                <i class="fa fa-2x fa-hourglass-half"></i> Status: <a href="#" id="status">
                                    <b>{{$status}}</b>
                                </a>
                                <div class="show-status">
                                    <ul>
                                        <li>
                                            <a href="#" data-order-status="-1">Show all</a>
                                        </li>
                                        @foreach(\App\Invites::$status as $key => $status)
                                            <li>
                                                <a href="#" data-order-status="{{$key}}">{{$status}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <input type="hidden" name="status" id="status-order" value="{{$status_id}}">
                        </form>
                    </div>
                    <div class="table-orders">
                        <a href="#" class="table-row test-tutorial" data-step="2" >
                            <div class="name col">
                                <span>friend@email.com</span>
                                <p class="date-mobile">{{\Carbon\Carbon::now()->format('m/d/Y')}}</p>
                            </div>
                            <div class="date col">{{\Carbon\Carbon::now()->format('m/d/Y')}}</div>
                            <div class="status col ">During</div>
                            <div class="balance col">$0</div>
                        </a>
                        @foreach($invites as $invite)
                            <a href="#{{$invite->id}}" class="table-row">
                                <div class="name col">
                                    <span>{{$invite->email}}</span>
                                    <p class="date-mobile">{{Carbon\Carbon::parse($invite->created_at)->format('m/d/Y')}}</p>
                                </div>
                                <div class="date col">{{Carbon\Carbon::parse($invite->created_at)->format('m/d/Y')}}</div>
                                <div class="status col @if($invite->status != 0) complete @endif">{{\App\Invites::$status[$invite->status]}}</div>
                                <div class="balance col">${{$invite->paid}}</div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
    <script>
        if(!getCookie('invite'))
            startTutorialInvite();
    </script>
@endsection