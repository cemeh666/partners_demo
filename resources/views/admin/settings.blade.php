<div class="content body">

    <form class="panel panel-default" method="POST" action="/admin/settings">
        <div class="form-elements">
            <div class="panel-body">
                <div class="form-elements">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-elements">
                                <div class="form-group form-element-text ">
                                    <label for="partners" class="control-label">
                                        Count partners
                                    </label>
                                    <input class="form-control" type="number" id="partners" name="count_partners" value="{{$settings->count_partners}}">
                                </div>
                                <div class="form-group form-element-password ">
                                    <label for="cache" class="control-label">
                                        Count Cache-back
                                    </label>
                                    <input class="form-control" type="number" id="cache" name="count_cache" value="{{$settings->count_cache}}">
                                </div>
                                <div class="form-group form-element-password ">
                                    <label for="donate" class="control-label">
                                        Count Donate
                                    </label>
                                    <input class="form-control" type="number" id="donate" name="count_donate" value="{{$settings->count_donate}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-elements">
                                <div class="form-group form-element-text">
                                    <label for="paid" class="control-label">
                                        Min sum paid
                                    </label>
                                    <input class="form-control" type="number" id="paid" name="min_paid" value="{{$settings->min_paid}}">
                                </div>
                            </div>
                            <div class="form-elements">
                                <div class="form-group form-element-text">
                                    <label for="price_invite" class="control-label">
                                        Paid for Invite user
                                    </label>
                                    <input class="form-control" type="number" id="price_invite" name="price_invite" value="{{$settings->price_invite}}">
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="form-buttons panel-footer">
            <div class="btn-group" role="group">
                <button type="submit" name="next_action" class="btn btn-primary" value="save_and_continue">
                    <i class="fa fa-check"></i> Save
                </button>
            </div>
        </div>
    </form>


</div>
