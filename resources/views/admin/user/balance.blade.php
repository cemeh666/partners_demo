<div class="content body panel panel-default">

<table class="table-primary table table-striped">
    @foreach($history as $item)
        <tr>
            <td class="name col">
                <span>{{$item->name}}</span>
            </td>
            <td>{{Carbon\Carbon::parse($item->created_at)->format('m/d/Y')}}</td>
            <td class="@if($item->type == 1) text-success @else text-danger @endif">{{\App\BalanceHistory::$type[$item->type]}}</td>
            <td class="@if($item->type == 1) text-success @else text-danger @endif">${{$item->count}}</td>
        </tr>
    @endforeach</table>


</div>
