<div class="col-md-4">
    <!-- Bar chart -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-bar-chart-o"></i>

            <h3 class="box-title">Views</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div id="bar-chart" style="height: 300px; padding: 0px; position: relative;">
            </div>
        </div>
        <!-- /.box-body-->
    </div>
</div>
    <div class="col-md-3">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>{{$users_count}}</h3>

                <p>User Registrations</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-plus"></i>
            </div>
            @if(Auth::user()->checkModels(\App\User::class))
                <a href="{{AdminNavigation::getPages()->get(\App\User::class)->getUrl()}}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            @endif
        </div>

    </div>
    <!-- /.box -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="https://adminlte.io/themes/AdminLTE/bower_components/Flot/jquery.flot.js"></script>
    <script type="text/javascript" src="https://adminlte.io/themes/AdminLTE/bower_components/Flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="https://adminlte.io/themes/AdminLTE/bower_components/Flot/jquery.flot.categories.js"></script>
    <script>
        /*
         * BAR CHART
         * ---------
         */

        var bar_data = {

//            data: [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]],
            data: <?= \App\Views::getLastWeek(); ?>,
            color: "#3c8dbc"
        };
        $.plot("#bar-chart", [bar_data], {
            grid: {
                borderWidth: 1,
                borderColor: "#f3f3f3",
                tickColor: "#f3f3f3",
                hoverable: true,
            },
            series: {
                bars: {
                    show: true,
                    barWidth: 0.5,
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0
            },
        });
        //Initialize tooltip on hover
        $('<div class="tooltip-inner" id="line-chart-tooltip"></div>').css({
            position: "absolute",
            display: "none",
            opacity: 0.8
        }).appendTo("body");
        $("#bar-chart").bind("plothover", function (event, pos, item) {

            if (item) {
                var y = item.datapoint[1];

                $("#line-chart-tooltip").html(y+" Views")
                    .css({top: item.pageY - 30, left: item.pageX-30})
                    .fadeIn(200);
            } else {
                $("#line-chart-tooltip").hide();
            }
        });

        /* END BAR CHART */
    </script>