<div class="content body">
    <style type="text/css">
        input{
            width: 300px;
            margin: 0 auto;
        }
    </style>
    <form class="panel panel-default" method="POST" action="/admin/insurance" enctype="multipart/form-data">
        <div class="form-elements">
            <div class="panel-body">
                <div class="form-elements">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-elements">
                                <div class="form-group form-element-wysiwyg ">

                                    <textarea class="form-control" id="text" name="text" cols="50" rows="10">{!! $insurance->text !!}</textarea>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <div class="form-group form-element-file ">
                        <input type="text" name="name1" class="form-control" placeholder="name"  style="width: 300px; margin: 5px auto;"
                        value="{{$insurance->name1}}">

                        <label for="file1" class="btn btn-primary upload-button dz-clickable">
                            <i class="fa fa-upload"></i> Select File
                        </label>

                        <input type="file" name="file1" value="" class="hidden ins-file" id="file1">
                        <p class="file-name">
                            @if($insurance->file1)
                                <a href="{{$insurance->file1}}" target="_blank"> Download.{{explode('.', $insurance->file1)[1]}}</a>
                                <a href="/admin/insurance/1/delete"> <i class="fa fa-close text-danger"></i></a>
                            @endif
                        </p>
                    </div>
                </div>

                <div class="col-md-4 text-center">
                    <div class="form-group form-element-file ">
                        <input type="text" name="name2" class="form-control" placeholder="name" style="width: 300px; margin: 5px auto;"
                               value="{{$insurance->name2}}">
                        <label for="file2" class="btn btn-primary upload-button dz-clickable">
                            <i class="fa fa-upload"></i> Select File
                        </label>

                        <input type="file" name="file2" value="" class="hidden ins-file" id="file2">
                        <p class="file-name">
                            @if($insurance->file2)
                                <a href="{{$insurance->file2}}" target="_blank"> Download.{{explode('.', $insurance->file2)[1]}}</a>
                                <a href="/admin/insurance/2/delete"> <i class="fa fa-close text-danger"></i></a>
                            @endif
                        </p>
                    </div>
                </div>

                <div class="col-md-4 text-center">
                    <div class="form-group form-element-file ">
                        <input type="text" name="name3" class="form-control" placeholder="name"  style="width: 300px; margin: 5px auto;"
                               value="{{$insurance->name3}}">

                        <label for="file3" class="btn btn-primary upload-button dz-clickable">
                            <i class="fa fa-upload"></i> Select File
                        </label>

                        <input type="file" name="file3" value="" class="hidden ins-file" id="file3">
                        <p class="file-name">
                            @if($insurance->file3)
                                <a href="{{$insurance->file3}}" target="_blank">Download.{{explode('.', $insurance->file3)[1]}}</a>
                                <a href="/admin/insurance/3/delete" > <i class="fa fa-close text-danger"></i></a>
                            @endif
                        </p>
                    </div>
                </div>

            </div>
        </div>


        <div class="form-buttons panel-footer">
            <div class="btn-group" role="group">
                <button type="submit" name="next_action" class="btn btn-primary" value="save_and_continue">
                    <i class="fa fa-check"></i> Save
                </button>
            </div>
        </div>
    </form>
    <script src="/packages/sleepingowl/default/js/admin-app.js"></script>
    <script src="//cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

    <script>

        Admin.WYSIWYG.switchOn('text', 'ckeditor',
            {
                "height":400,
                "contentsCss":['/css/app.css', '/css/style.css', '/css/fonts/roboto.css'],
                'allowedContent':true,
                'filebrowserUploadUrl': '/uploader'
            });

        $(".ins-file").each(function(i, file) {
            file.addEventListener("change", function(e) {
                change_file(this.files[0], $(this));
            });
        });

        function change_file(file, $this) {
            var label = $this.parent().find('.file-name');
            label.html('<i class="fa fa-paperclip"></i> '+file.name);
        }
    </script>

</div>
