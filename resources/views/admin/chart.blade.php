<div class="content body">

    <form class="panel panel-default" method="POST" action="/admin/chart">
        <div class="form-elements">
            <div class="panel-body">
                <div class="form-elements">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-elements">
                                <div class="form-group form-element-wysiwyg ">

                                    <textarea class="form-control" id="text" name="text" cols="50" rows="10">{!! $chart->text !!}</textarea>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="form-buttons panel-footer">
            <div class="btn-group" role="group">
                <button type="submit" name="next_action" class="btn btn-primary" value="save_and_continue">
                    <i class="fa fa-check"></i> Save
                </button>
            </div>
        </div>
    </form>
    <script src="/packages/sleepingowl/default/js/admin-app.js"></script>
    <script src="//cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

    <script>

        Admin.WYSIWYG.switchOn('text', 'ckeditor',
            {
                "height":500,
                "contentsCss":['/css/app.css', '/css/style.css', '/css/fonts/roboto.css'],
                'allowedContent':true,
                'filebrowserUploadUrl': '/uploader'
            });
    </script>

</div>
