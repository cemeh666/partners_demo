@extends('layouts.lk')

@section('content')
    {!! $insurance->text !!}
<div class="row insurance">

    <div class="container">
        <div class="row">
            <div class="files-block">
                @if($insurance->file1)
                    <div class="col-md-4 col-xs-4">
                        <a href="{{$insurance->file1}}" target="_blank">
                            <div class="insurance-block">
                                <p>{{$insurance->name1}}</p>
                                <div class="pdf">
                                    <img src="/images/front/insurance-pdf.png" alt="">
                                </div>
                            </div>
                        </a>
                    </div>
                @endif

                @if($insurance->file2)
                    <div class="col-md-4 col-xs-4">
                        <a href="{{$insurance->file2}}" target="_blank">
                            <div class="insurance-block">
                                <p>{{$insurance->name2}}</p>
                                <div class="pdf">
                                    <img src="/images/front/insurance-pdf.png" alt="">
                                </div>
                            </div>
                        </a>
                    </div>
                @endif

                @if($insurance->file3)
                    <div class="col-md-4 col-xs-4">
                        <a href="{{$insurance->file3}}" target="_blank">
                            <div class="insurance-block">
                                <p>{{$insurance->name3}}</p>
                                <div class="pdf">
                                    <img src="/images/front/insurance-pdf.png" alt="">
                                </div>
                            </div>
                        </a>
                    </div>
                @endif
            </div>
            <div class="form-block">
                <h3>Add an Additional Insured</h3>

                <form action="/api/insurance" method="post" data-form-insurance class="form-horizontal">
                    <div class="form-group">
                        <label for="name" class="control-label">Name</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="e.g. John Malkovich" required="true">
                    </div>
                    <div class="form-group">
                        <label for="address" class="control-label">Address</label>
                        <textarea name="address" id="address" class="form-control"  placeholder="e.g. 1234 Hollywood Boulevard, Los Angeles, CA, United States, apt. 26" required="true"></textarea>
                    </div>
                    <button>Send</button>
                </form>
            </div>


        </div>
    </div>
</div>

@endsection
