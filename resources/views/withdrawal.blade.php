@extends('layouts.lk')

@section('content')

    <section class="profile-body">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    @include('layouts._order')
                <div class="items">
                    <div class="title">
                        Balance History:
                    </div>
                    <div class="table-head">
                        <form action="/withdrawal" method="get" class="profile-order">
                            <div class="filter-name">
                                <i class="fa fa-search search"></i>
                                <input type="text" name="search" placeholder="Name:" class="form-control" value="{{$search}}">
                            </div>
                            <div  class="filter-date">
                                <button style="background: transparent"><i class="fa fa-2x fa-calendar-o"></i></button>
                                <input type="text" name="date_order" placeholder="filter with date" data-placeholder="filter with date" class="cal" id="date_order" value="{{$date}}">
                                @if($date)<button class="fa fa-close text-danger date-reset"></button>@endif
                            </div>
                            <div class="filter-status">
                                <i class="fa fa-2x fa-hourglass-half"></i> Status: <a href="#" id="status">
                                    <b>{{$status}}</b>
                                </a>
                                <div class="show-status">
                                    <ul>
                                        <li>
                                            <a href="#" data-order-status="-1">Show all</a>
                                        </li>
                                        @foreach(\App\BalanceHistory::$type as $key => $status)
                                            <li>
                                                <a href="#" data-order-status="{{$key}}">{{$status}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <input type="hidden" name="status" id="status-order" value="{{$status_id}}">

                        </form>
                    </div>
                    <div class="table-orders">
                        @foreach($history as $item)
                            <a href="#"  class="table-row">
                                <div class="name col">
                                    <span>{{$item->name}}</span>
                                    <p class="date-mobile">{{Carbon\Carbon::parse($item->created_at)->format('m/d/Y')}}</p>
                                </div>
                                <div class="date col">{{Carbon\Carbon::parse($item->created_at)->format('m/d/Y')}}</div>
                                <div class="status col @if($item->type == 1) complete @else paid @endif">{{\App\BalanceHistory::$type[$item->type]}}</div>
                                <div class="balance col @if($item->type == 1) complete @else paid @endif">${{$item->count}}</div>
                            </a>
                        @endforeach
                    </div>

                </div>

            </div>
            </div>

        </div>
    </section>

@endsection
