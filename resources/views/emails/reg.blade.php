<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        #tip a {
            color: #00acd6;
        }
    </style>
</head>
<body cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"
      style="margin-top:0; margin-left: 0; margin-bottom: 0; margin-right: 0;
           padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0;
           font-family: Arial, Helvetica, sans-serif;">

<table height="100%" width="600px" cellpadding="0" cellspacing="0" border="0" style="width: 600px; margin: auto;">
    <tr>
        <td style="text-align: center; padding: 10px">
            Your success registration "Partners!"
        </td>
    </tr>
    <tr>
        <td style="text-align: center; padding: 10px">
            <span>Your password: <strong>{{$password}}</strong></span>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; padding: 10px">
            <a href="{{$link}}">Enter site</a>
        </td>
    </tr>

</table>
</body>
</html>