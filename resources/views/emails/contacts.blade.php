@extends('layouts.email')

@section('content')
<table class="wrapper_table" mc:repeatable width="800" cellspacing="0" cellpadding="0" border="0" align="center"
       style="max-width: 800px">
    <tr>
        <td class="noresponsive">
            <table cellpadding="0" cellspacing="0" border="0" align="center" width="800px">
                <tr>
                    <td cellpadding="0" cellspacing="0" border="0" style="line-height:1px;"
                        height="1px; min-width: 800px;"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table class="wrapper_table" style="max-width: 800px;" width="800" cellspacing="0" cellpadding="0" border="0"
       align="center">
    <tbody>
    <tr>
        <td class="content blue_bg" style="background: rgb(103, 167, 80) none repeat scroll 0% 0%;" width="800"
            bgcolor="#67a750">
            <table cellspacing="0" cellpadding="0" border="0">
                <tbody>
                <tr>
                    <td colspan="3" class="height40" height="70"></td>
                </tr>
                <tr>
                    <td class="padding" width="165"></td>
                    <td class="content_row" width="470" align="center">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="white_bg content_row" style="background: #ffffff;" width="455"
                                                align="center">
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" cellspacing="0" cellpadding="0"
                                                                   border="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td class="padding" width="40"></td>
                                                                    <td align="center">
                                                                        <table width="100%" cellspacing="0"
                                                                               cellpadding="0" border="0">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td height="40"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center"><a
                                                                                            href="https://partners.loyalmoving.com/"
                                                                                            target="_blank"
                                                                                            style="text-decoration: none;"><img
                                                                                                src="https://partners.loyalmoving.com/images/front/logo.png"
                                                                                                alt="" style="display: block;"
                                                                                                class="" title="" width="120"
                                                                                                height="40" border="0"></a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="" height="20"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="blue_text weight_700 raleway editing-element"
                                                                                    style="font-size: 30px; line-height: 25px; font-weight: 700; font-family: &quot;Raleway&quot;,sans-serif; color: rgb(91, 148, 71);"
                                                                                    align="center">New form contacts
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="" height="15"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="blue_text raleway weight_600"
                                                                                    style="font-size: 16px; line-height: 20px; font-weight: 600; font-family: &quot;Raleway&quot;,sans-serif; color: rgb(91, 148, 71);"
                                                                                    align="center">
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="" height="25"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="roboto weight_400 dark_grey_text"    style="font-size: 15px; line-height: 20px; font-weight: 400; font-family: 'Roboto', sans-serif; color: #464646;"  align="center">
                                                                                        Name: <strong>{{$contacts->name}}</strong><br>
                                                                                        Email: <strong>{{$contacts->address}}</strong><br>
                                                                                        Phone: <strong>{{$contacts->phone}}</strong><br>
                                                                                        Comments: <strong>{{$contacts->comments}}</strong>

                                                                            </tr>
                                                                            <tr>
                                                                                <td class="roboto weight_400 dark_grey_text"    style="font-size: 15px; line-height: 20px; font-weight: 400; font-family: 'Roboto', sans-serif; color: #464646;"  align="left">

                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="" height="25"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table cellspacing="0"
                                                                                           cellpadding="0" border="0">
                                                                                        <tbody>
                                                                                        <tr></tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="" height="35"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="light_grey_text weight_400 roboto"
                                                                                    style="font-size: 11px; line-height: 15px; font-weight: 400; font-family: 'Roboto', sans-serif; color: #acadae;"
                                                                                    align="center">
                                                                                    All the best<br>
                                                                                    Partners support team<br>
                                                                                    � 2017 Loyal Moving
                                                                                    Partners. All rights reserved.
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="space_class"
                                                                                    style="font-size: 1px; line-height: 1px;"
                                                                                    height="10">&nbsp;</td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                    <td class="padding" width="39"></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td class="nomobile" width="15">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td class="blue_bg"
                                                            style="background: rgb(103, 167, 80) none repeat scroll 0% 0%;"
                                                            width="15" height="15" bgcolor="#67a750"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="blue_bg10"
                                                            style="background: rgb(89, 144, 69) none repeat scroll 0% 0%;"
                                                            width="15" height="470" bgcolor="#599045"></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="nomobile">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td width="15" height="15"></td>
                                            <td class="blue_bg10 space_class"
                                                style="font-size: 1px; line-height: 1px; background: rgb(89, 144, 69) none repeat scroll 0% 0%;"
                                                height="15" bgcolor="#599045">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="height40" height="60"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="padding" width="165"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
@endsection