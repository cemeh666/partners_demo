<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->float('balance')->default(0);
            $table->string('info')->nullable();
            $table->string('email');
            $table->string('name')->nullable();
            $table->integer('status');
            $table->boolean('copy')->default(false);
            $table->string('phone');
            $table->string('size')->nullable();
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->date('date')->nullable();
            $table->string('time')->nullable();
            $table->integer('work')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('commission')->nullable();
            $table->string('address', 255);
            $table->string('file')->nullable();
            $table->string('check')->nullable();
            $table->text('comment')->nullable();
            $table->integer('customer_saved')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
