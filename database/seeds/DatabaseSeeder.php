<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
//        \App\User::truncate();
//        \App\Role::truncate();
//        DB::table('role_user')->truncate();
        if(!\App\User::where('email', 'admin@admin.admin')->first()){
            $adminUser = \App\User::create([
                'name'     => 'admin',
                'email'    => 'admin@admin.admin',
                'password' => 'admin@admin.admin',
                'avatar' => 'images/admin/admin.png',
                'promo' => 'LM123ADMIN',
            ]);

            $adminRole = \App\Role::create([
                'name'  => 'admin',
                'label' => 'Administrator'
            ]);

            $adminUser->roles()->attach($adminRole);
        }
        \App\Settings::truncate();
        \App\Settings::create([
            "count_partners" => 154,
            "count_cache"    => 78854,
            "count_donate"   => 67,
            "min_paid"       => 100,
            "price_invite"   => 50,
        ]);

        \App\Insurance::truncate();
        \App\Insurance::create([
            'text' => '
            <div class="insurance">
            <div class="container">
            <h2 class="title">Insurance:</h2>
            <div class="insurance-content">
            <div class="row">
                <div class="col-md-4 image">
                    <img src="/images/admin/insurance.jpg" alt="">
                </div>
                <div class="col-md-8 text">
                    Sentem, unum mandem dienat L. Haberudemus mus cavoludam a nius, vendacerrit pultort ilicitrum tante et C. Us perur locci comnini ridet? Iquostisus it grae ervit, dii publiu quit. Num et? It fauci se audemum, unterfi tabefecone ausupio nsupimilin voccisse castervidem muntertereis adhum et re horimusque quemus, non sultortis me cons estris.Essa numususum imurem rebus. Ser pata vit; non ta mervivi lincenatium mor pra, consum que moribenatam rebat, vcuteni fex moenihil unum et furatia diis Catilic ta, ur, que me tum etestra? O tastrarte, con verfita rteluscem hoc, ocullego in retiae auro hae condi ca quonem mei tiqui foratia suppl. Denessidetin se conem peri iam fauderceri praes, contur autercesus essitr Ser pata vit; non ta mervivi lincenatium mor pra, consum que moribenatam rebat, vcuteni fex moenihil unum et furatia diis Catilic ta, ur, que me tum etestra? O tastrarte, con verfita rteluscem hoc, ocullego in retiae auro hae condi ca quonem mei tiqui foratia suppl. Denessidetin se conem peri iam fauderceri praes, contur autercesus essitr
                </div>
            </div>
        </div>
        </div>
        </div>'
        ]);
        \App\Chart::truncate();
        \App\Chart::create([
            'text' => '
            <div class="insurance">
            <div class="container">
            <h2 class="title">Commission chart:</h2>
            <div class="insurance-content">
            <div class="row">
                <div class="col-md-4 image">
                    <img src="/images/admin/insurance.jpg" alt="">
                </div>
                <div class="col-md-8 text">
                    Sentem, unum mandem dienat L. Haberudemus mus cavoludam a nius, vendacerrit pultort ilicitrum tante et C. Us perur locci comnini ridet? Iquostisus it grae ervit, dii publiu quit. Num et? It fauci se audemum, unterfi tabefecone ausupio nsupimilin voccisse castervidem muntertereis adhum et re horimusque quemus, non sultortis me cons estris.Essa numususum imurem rebus. Ser pata vit; non ta mervivi lincenatium mor pra, consum que moribenatam rebat, vcuteni fex moenihil unum et furatia diis Catilic ta, ur, que me tum etestra? O tastrarte, con verfita rteluscem hoc, ocullego in retiae auro hae condi ca quonem mei tiqui foratia suppl. Denessidetin se conem peri iam fauderceri praes, contur autercesus essitr Ser pata vit; non ta mervivi lincenatium mor pra, consum que moribenatam rebat, vcuteni fex moenihil unum et furatia diis Catilic ta, ur, que me tum etestra? O tastrarte, con verfita rteluscem hoc, ocullego in retiae auro hae condi ca quonem mei tiqui foratia suppl. Denessidetin se conem peri iam fauderceri praes, contur autercesus essitr
                </div>
            </div>
        </div>
        <div class="row">
        <div class="col-md-4 hidden-xs">
        <div class="block">
        <div class="plus"></div>
        
        <div class="upload"></div>
        </div>
        </div>
        
        <div class="col-md-4 hidden-xs">
        <div class="block">
        <div class="plus"></div>
        
        <div class="upload"></div>
        </div>
        </div>
        
        <div class="col-md-4 hidden-xs">
        <div class="block">
        <div class="plus"></div>
        
        <div class="upload"></div>
        </div>
        </div>
        </div>
        </div>
        </div>'
        ]);
    }
}
