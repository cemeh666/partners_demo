<?php

use Illuminate\Database\Seeder;

class TextSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('text_editor')->truncate();

        $items = [
            [
                'id' => 1,
                'text_id' => 'text1',
                'text' => '<h2 class="title"> WANT TO GENERATE A PASSIVE INCOME? </h2> <div class="body"> Become a partner <strong>with loyal moving</strong>, refer a client and get up to <strong>15% commision</strong> </div> <button id="sing" data-toggle="modal" data-target="#signUpModal">SIGN UP</button>',
            ],
            [
                'id' => 2,
                'text_id' => 'our_partners',
                'text' =>'<div class="container"><h2 class="title"> OUR PARTNERS </h2> <div class="delimiter"></div> <div class="row top"> <div class="partner pull-left"> <div class="text text-right"> <h3 class="title">CONTRACTERS</h3> <p>Lorem ipsum dolor</p> </div> <div class="delimiter"></div> <div class="image"> <img src="/images/front/contracter.png" alt=""> </div> </div> <div class="partner pull-right "> <div class="image"> <img src="/images/front/lawyers.png" alt=""> </div> <div class="delimiter"></div> <div class="text text-left"> <h3 class="title text-left">LAWYERS</h3> <p>Aenean euismod bibendum laoreet.</p> </div> </div> </div> <div class="row bottom"> <div class="partner pull-left"> <div class="text text-right"> <h3 class="title">REALTORS</h3> <p>Aenean euismod bibendum  laoreet.</p> </div> <div class="delimiter"></div> <div class="image"> <img src="/images/front/realtor.png" alt=""> </div> </div> <div class="partner pull-right"> <div class="image"> <img src="/images/front/manager.png" alt=""> </div> <div class="delimiter"></div> <div class="text text-left"> <h3 class="title">APARTMENT MANAGERS</h3> <p>Lorem ipsum dolor sit amet, consectetur</p> </div> </div> </div> <div class="arrow"> <i class="fa fa-angle-down"></i> </div> </div>'
            ],
            [
                'id' => 3,
                'text_id' => 'works',
                'text' =>'<div class="container"> <h2 class="title"> HOW IT WORKS </h2> <div class="text-center"> <div class="refer"> <img src="/images/front/works_top.png" alt=""> <div class="text"> <div class="title">REFER</div> <div class="body"> "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </div> </div> </div> <i class="fa fa-angle-down"></i> <div class="moving"> <div class="text"> <div class="title">LOYAL MOVING</div> <div class="body"> "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </div> </div> <img src="/images/front/moving.png" alt=""> <div class="text-mobile"> <div class="title">LOYAL MOVING</div> <div class="body"> "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </div> </div> </div> <i class="fa fa-angle-down"></i> <div class="row"> <div class="partner"> <img src="/images/front/getpaid.png" alt=""> <p>Partner</p> </div> <div class="donate"> <img src="/images/front/heart.png" alt=""> <p>Donate</p> </div> </div> </div> </div> '
            ],
            [
                'id' => 4,
                'text_id' => 'donate',
                'text' => ' <h2 class="title"> SIGN UP & DONATE </h2> <div class="donate-monet"> <form action="/api/registration" method="post" data-form-sing> <div class="form-group"> <input type="text" class="form-control" name="name" placeholder="Enter Your Name:" required="true"> </div> <div class="form-group"> <input type="email" class="form-control" name="email" placeholder="Enter Your Email Address:" required="true"> </div> <div class="form-group text-center"> <button>SIGN UP</button> </div> <a href="#">Read More</a> </form> </div>',
            ],
        [
                'id' => 5,
                'text_id' => 'statistic_text',
                'text' => 'Sentem, unum mandem dienat L. Haberudemus mus cavoludam a nius, vendacerrit pultort ilicitrum tante et C. Us perur locci comnini ridet? Iquostisus it grae ervit, dii publiu quit. Num et? It fauci se audemum, unterfi tabefecone ausupio nsupimilin voccisse castervidem muntertereis adhum et re horimusque quemus, non sultortis me cons estris.Essa numususum imurem rebus. Ser pata vit; non ta mervivi lincenatium mor pra, consum que moribenatam rebat, vcuteni fex moenihil unum et furatia diis Catilic ta, ur, que me tum etestra?',
            ],
        ];
        foreach($items as $item) {
            DB::table('text_editor')->insert($item);
        }
    }
}
