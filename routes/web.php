<?php

Route::group(['prefix' => '',  'middleware'=>['auth','lk']], function() {
    Route::get('/profile', 'IndexController@profile');
    Route::get('/invites', 'IndexController@invites');
    Route::get('/insurance', 'IndexController@insurance');
    Route::get('/withdrawal', 'IndexController@withdrawal');
    Route::get('/chart', 'IndexController@chart');
    Route::get('/order/{id}/archive', 'IndexController@order_archive');
    Route::get('/order/{id}/restore', 'IndexController@order_restore');


    Route::post('/profile/order', 'IndexController@profile_order');
    Route::post('/send/invite', 'IndexController@send_invite');
    Route::post('/order/{id}/update', 'ApiController@order_update');
    Route::post('/balance/withdrawal/{type}', 'ApiController@withdrawal');
});

Route::get('/', 'IndexController@index');
Route::get('/email/auth', 'ApiController@auth_email');
Route::get('/settings', 'IndexController@settings')->middleware('auth');
Route::post('/settings', 'IndexController@settings_save')->middleware('auth');
Route::post('/reset/password', 'ApiController@reset_password');


//Route::group(['prefix' => '/admin', 'namespace'  => 'Admin', 'middleware'=>'auth'], function() {
//    Route::post('settings', 'AdminController@settings');
//});

//    Route::get('/home', 'HomeController@index');

Auth::routes();
Route::any('/logout','\App\Http\Controllers\Auth\LoginController@logout');
Route::post('/send/contact', 'IndexController@send_contact');

Route::group(['prefix' => '/api'], function(){
    Route::get('/promo/{promo}', 'ApiController@promo_order');
    Route::post('/registration', 'ApiController@registration');
    Route::post('/promo', 'ApiController@promo');
    Route::post('/insurance', 'ApiController@insurance');
    Route::post('/auth', 'ApiController@auth');
    Route::post('/save/editable', function (\Illuminate\Http\Request $request){
        if(\Auth::user()->isSuperAdmin()){
            \App\TextEditor::saveText($request->id, $request->get('content'));
        }
    });
});

Route::any('/uploader',function (){
    $callback = $_GET['CKEditorFuncNum'];
    $error = '';
    $file = \Request::file('upload'); //Сам файл
    $filename = md5(date("YmdHis").rand(5,50));
    $extension = $file->getClientOriginalExtension();
    $file->move(public_path('/cke-uploaded/'), $filename.'.'.$extension);
    $http_path = '/cke-uploaded/'.$filename.".".$extension;
    echo "<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction(".$callback.",  \"".$http_path."\", \"".$error."\" );</script>";
});
