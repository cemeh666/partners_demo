/**
 * Created by cemeh666 on 12.06.17.
 */
    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function setCookie(name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires*24*60*60 * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }

    function deleteCookie(name) {
        setCookie(name, "", {
            expires: -1
        })
    }
function startTutorialProfile() {
    if(device.desktop()) {
        var tutorial = introJs();

        tutorial.setOptions({
            'showProgress': true,
            'showBullets': false,
            steps: [
                {
                    intro: "<h3>How it works?</h3> Lets say your client/tenant John Malkovich is moving"
                },
                {
                    element: document.querySelector('[data-step="1"]'),
                    intro: "Fill John Malkovich's info here",
                    position: 'left'
                },
                {
                    element: document.querySelector('[data-step="2"]'),
                    intro: "Or simply send the following unique personal promo code to John Malkovich, which he can use in our website to get discount.",
                    position: 'bottom'
                },
                {
                    element: document.querySelector('[data-step="3"]'),
                    intro: 'Monitor John Malkovich\'s job/quote status and the commission you earned here',
                    position: 'bottom'
                },
                {
                    element: document.querySelector('[data-step="4"]'),
                    intro: "Redeem Your commission any time and any way you prefer",
                    position: 'bottom'
                }

            ]
        });

        tutorial.onexit(function() {
            setCookie('profile', 1, {expires:365});
        });
        tutorial.start();
    }

}

function startTutorialInvite() {

    if(device.desktop()) {
        $('.test-tutorial').css('display','block');
        var tutorial = introJs();

        tutorial.setOptions({
            'showProgress': true,
            'showBullets': false,
            steps: [
                {
                    intro: "Refer your friend to join our professional network and get $50.00 when they refer their first client."
                },
                {
                    element: document.querySelector('[data-step="1"]'),
                    intro: "Enter your friends email address here",
                    position: 'left'
                },
                {
                    element: document.querySelector('[data-step="2"]'),
                    intro: "Monitor the status here",
                    position: 'bottom'
                }
            ]
        });

        tutorial.onexit(function() {
            setCookie('invite', 1, {expires:365});
            $('.test-tutorial').hide();
        });
        tutorial.start();
    }

}