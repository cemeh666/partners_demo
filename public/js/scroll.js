/**
 * Created by cemeh666 on 30.06.17.
 */
$( document ).ready(function() {

    var hash = location.hash;
    if (hash) {
        $("[href='" + hash + "']").addClass('active');
    } else
        $("[href='#top']").addClass('active');

    var anchors = [];
    var currentAnchor = 0;
    var isAnimating = false;

    var mousewheelevt=(/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel"; //FF doesn't recognize mousewheel as of FF3.x

    // if (document.attachEvent) //if IE (and Opera depending on user setting)
    //     document.attachEvent("on"+mousewheelevt, function(e){alert('Mouse wheel movement detected!')});
    // else if (document.addEventListener) //WC3 browsers
    //     document.addEventListener(mousewheelevt, function(e){alert('Mouse wheel movement detected!')}, false);
    $(function () {
        $("a[href*='#']:not([href='#'])").click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                $(this).parent().parent().find('li a').removeClass('active');
                $(this).addClass('active');

                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    location.hash = target[0].id;
                    updateAnchors();
                }
            }
        });
        function updateAnchors() {
            anchors = [];
            $('section').each(function (i, element) {
                if (element.id == location.hash.replace('#', ''))
                    currentAnchor = i;
                anchors.push($(element));
            });
        }

        $('body').on(mousewheelevt, function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (isAnimating) {
                return false;
            }
            isAnimating = true;
            // Increase or reset current anchor
            var delta = (/Firefox/i.test(navigator.userAgent)) ? e.detail <= 0 : e.originalEvent.wheelDelta >= 0;
            if (delta) {
                currentAnchor--;
            } else {
                currentAnchor++;
            }
            if (currentAnchor > (anchors.length - 1)
                || currentAnchor < 0) {
                currentAnchor = 0;
            }
            isAnimating = true;
            $('html, body').animate({
                scrollTop: parseInt(anchors[currentAnchor].offset().top)
            }, 1000, 'swing', function () {
                isAnimating = false;
            });
            var id = anchors[currentAnchor].attr('id');
            $('.dots').find('li a').removeClass('active');
            location.hash = id;
            $("[href='#" + id + "']").addClass('active');
        });

        updateAnchors();
    });
});