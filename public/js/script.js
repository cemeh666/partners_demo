/**
 * Created by cemeh666 on 12.06.17.
 */
$( document ).ready(function() {
    $('.time-block').hide();

    $("input[name='phone']").mask("+1 (999) 999-9999");
    try{
        $(".geocomplete").geocomplete();

    }catch (e){
        console.log(e);
    }
    $("input[name='info']").on("change", function () {
        if($(this).val() == 'On site estimate'){
            $('.time-block').show().find('input').attr('required', true);
            $('.data-block').removeClass('col-xs-12').addClass('col-xs-7');
        }
        else{
            $('.data-block').removeClass('col-xs-7').addClass('col-xs-12');
            $('.time-block').hide().find('input').removeAttr('required').val('');
        }
    });
    var status = 1;
    $("#status").on('click', function (e) {
        e.preventDefault();
        if(status)
            $(".show-status").show();
        else
            $(".show-status").hide();
        status = !status;
    });
    $('[data-toggle="modal"]').on('click', function () {
        $('.modal').modal('hide');
    });
        $("body").on('click', function (e) {
            try{

                if(e.target.parentElement.className != 'show-status' || e.target.className != 'show-status'){
                if(e.target.nodeName != 'B'){
                    $(".show-status").hide();
                    status = 1;
                }
            }
            }catch (e){
                console.info(e);
            }
        });


    // if(device.desktop()) {
        cal_codeWidget({
            selector: ".calend",
            readonlyField: "full",
            cursorField: "pointer",
            dateFormat: "mm.dd.yyyy",
            position: "bottom right",
            minDate:1
        });
        cal_codeWidget({
            selector: ".cal",
            readonlyField: "full",
            cursorField: "pointer",
            dateFormat: "mm.dd.yyyy",
            position: "bottom left",
            minDate:0
        });
        try{
            $("[name=time]").timepicker({
                lang : {AM: 'AM', PM: 'PM'}
            });
        }catch (e){}

    // }
    if(device.tablet() || device.mobile()){
    //     $('.calend').attr('type','date');
    //     $('.cal').attr('type','date');
        $('[name="time"]').attr('type','time');
    }

    $('.paid-check').show();
    $('.paid-paypal').hide();
    $('.paid-donate').hide();
    $('.type input[type=radio]').on('change', function() {
        if(this.value == 1){
            $('.paid-check').show();
            $('.paid-paypal').hide();
            $('.paid-donate').hide();
        }
        if(this.value == 2){
            $('.paid-check').hide();
            $('.paid-paypal').show();
            $('.paid-donate').hide();
        }
        if(this.value == 3){
            $('.paid-check').hide();
            $('.paid-paypal').hide();
            $('.paid-donate').show();
        }
    });

    $( "[name=other]" ).click(function() {
        $('[name=specialist]').prop('checked', false);
    });

    function watchProperty(obj, name, handler) {
        if ('watch' in obj) {
            var old= obj.value;
            setInterval(function() {
                var n= obj.value;
                if (old!=n) {
                    old= handler.call(obj,name,old,n);
                }
            }, 200);
        } else if ('onpropertychange' in obj) {
            name= name.toLowerCase();

            obj.onpropertychange= function() {
                if (window.event.propertyName.toLowerCase()===name)
                    handler.call(obj);
            };

        } else {
            var o= obj['value'];
            setInterval(function() {
                var n= obj['value'];
                if (o!=n) {
                    o= handler.call(obj,name,o,n);
                }
            }, 200);
        }
    }
    try{
        watchProperty(document.getElementById('date_order'), 'value', function($this, prop, oldval, val) {
            console.log($this, prop, oldval, val);
            if (typeof(oldval) != "undefined" && oldval != ''){
                    $(".profile-order").submit();
            }
            return oldval ? oldval : '';
        });
    }catch(e) {}

    var toogle = false;
    $('.edit-order').on('click', function () {
        toogle = !toogle;
        $form = $(this).parent().parent().find('form');
        $form_id = $form.attr('data-form-update-order');
        if(toogle){
            $form.find('.info').find('.form-control').removeAttr('disabled').addClass('editable');
            $form.find('.label-file').show();
            $form.find('.no-file').show();
            $form.find('[name="from"]').addClass('geocomplete');
            $form.find('[name="to"]').addClass('geocomplete');
            $(".save-order-"+$form_id).show();
            $(".geocomplete").geocomplete();
        }else{
            $form.find('.info').find('.form-control').attr('disabled',true).removeClass('editable');
            $form.find('.label-file').hide();
            $form.find('.no-file').hide();
            $form.find('[name="from"]').removeClass('geocomplete');
            $form.find('[name="to"]').removeClass('geocomplete');
            $(".save-order-"+$form_id).hide();
        }



    });

    // try{
    //     var avatar = document.getElementById('avatar');
    //     avatar.addEventListener("change", function(e) {
    //         preview(this.files[0], '.avatar');
    //     });
    // }catch(e) {}

    $(".order-file").each(function(i, file) {
        file.addEventListener("change", function(e) {
            change_file(this.files[0], $(this));
        });
    });

    function change_file(file, $this) {
        var label = $this.parent().find('.file-name');
        label.html(file.name);
    }

    function preview(file, selector) {
        if ( file.type.match(/image.*/) ) {
            var reader = new FileReader(), img;
            reader.addEventListener("load", function(event) {
                img = document.createElement('img');
                img.src = event.target.result;
                $(selector).attr('src', img.src);
            });
            reader.readAsDataURL(file);
        }
    }
    $('.edit-order-field').on('click', function () {
        $(this).parent().find('input').removeClass('hidden');
        $(this).parent().find('p').addClass('hidden');
        var id = $(this).attr('data-form-id');
        $(".save-order-"+id).show();
    });
    $(".date-reset").on('click', function () {
        $(this).parent().find('input').val('');
        $(this).hide();
    });
    $('[data-order-status]').on('click', function () {
        $('#status-order').val($(this).attr('data-order-status'));
        $('.profile-order').submit();
    });

    $('.donate-company').on('click', function () {
        $('.donate-company').removeClass('active');
        $(this).addClass('active')
    });
    $( "#custom_name" ).click(function() {
        $(this).prop('required',true);
        $('.donate-company').removeClass('active');
        $('[name=donate]').prop('checked', false);
    });
    $('[name=donate]').on('click', function () {
        $("#custom_name").prop('required',false);
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
        var calendarTag = '.calendar-range';
        $(calendarTag).daterangepicker({
            "ranges": {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            autoUpdateInput: false,
            alwaysShowCalendars: true,
            opens: "center",
            locale: {
                cancelLabel: 'Clear',
                format: 'MM.DD.YYYY'
            }
        });
        $(calendarTag).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM.DD.YYYY') + ' - ' + picker.endDate.format('MM.DD.YYYY'));
        });

        $(calendarTag).on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
    $('[name="type_send[]"]').on('click', function () {
        if($(this).prop('checked') == true){
            $(this).parent().parent().find('[name="type_send[]"]').prop('required', false);
            $(this).parent().find('.form-control').prop('required', true);
            $(this).prop('required', true);
        }
        else{
            $(this).parent().find('.form-control').prop('required', false);
        }
    })
});