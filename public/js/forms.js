/**
 * Created by cemeh666 on 12.06.17.
 */
$( document ).ready(function() {

    $('[data-form-sing]').on("submit", function(e) {
        e.preventDefault();
        sendForm($(this), true, true);
        $(this).find('button').attr('disable','disable').addClass('disabled');
    });
    $('[data-form-auth]').on("submit", function(e) {
        $("#loaderModal").modal('show');
        e.preventDefault();
        sendForm($(this), false, false);
        $(this).find('button').attr('disable','disable').addClass('disabled');

    });
    $('[data-form-settings]').on("submit", function(e) {
        e.preventDefault();
        sendForm($(this), false, true);
        $(this).find('button').attr('disable','disable').addClass('disabled');
    });

    $('[data-form-orders]').on("submit", function(e) {
        $("#loaderModal").modal('show');
        e.preventDefault();
        sendForm($(this), true, true);
        $(this).find('button').attr('disable','disable').addClass('disabled');

    });

    $('[data-form-withdrawal]').on("submit", function(e) {
        e.preventDefault();
        sendForm($(this), true, true);
        $(this).find('button').attr('disable','disable').addClass('disabled');
    });

    $('[data-form-update-order]').on("submit", function(e) {
        e.preventDefault();
        sendForm($(this), true, true);
        $(this).find('button').attr('disable','disable').addClass('disabled');
    });

    $('[data-form-reset-password]').on("submit", function(e) {
        e.preventDefault();
        sendForm($(this), true, true);
        $(this).find('button').attr('disable','disable').addClass('disabled');
    });
    $('[data-form-promo]').on("submit", function(e) {
        e.preventDefault();
        sendForm($(this), true, true);
        $(this).find('button').attr('disable','disable').addClass('disabled');
    });

    $('[data-form-insurance]').on("submit", function(e) {
        e.preventDefault();
        sendForm($(this), true, true);
        $(this).find('button').attr('disable','disable').addClass('disabled');
    });
    function sendForm($this, refresh, success) {
        var url = $this.attr('action'),
            form = $this,
            data = new FormData(form[0]),
            timeout = 700,
            method = $this.attr('method');
        if($(form).attr('data-form-auth')){
            $("#signInModal").modal('hide');
            timeout = 0;
        }

        console.log(url, form, method);
        $.ajax({
            type: method,
            url: url,
            data: data,
            processData: false,  // Important!
            contentType: false,
            cache: false,
            success: function(msg) {
                data = JSON.parse(msg);
                if(data.status == 'Ok'){
                    if(success)
                        showSuccessModal(data, form[0]);
                    if(refresh)
                        $(form)[0].reset();
                    if(data.redirect){
                        setTimeout(function () {
                            window.location = data.redirect;
                        }, timeout)
                    }
                }else{
                    if($(form).attr('data-form-auth')){
                        $('.modal').modal('hide');
                        setTimeout(function () {
                            $("#signInModal").modal('show');
                        }, 300)
                    }
                    showFailedModal(data, form[0], false)
                }
                $(form[0]).find('button').removeAttr('disable').removeClass('disabled');


            },error: function (error) {
                $("#errorModal").modal('show');
                console.log(error);
            }
        });
    }

    function showSuccessModal(data, form) {
        var inputs = $(form).find('input');
        $.each(inputs, function() {
            $(this).removeClass('error-input');
            $(this).parent().find('.help-block').remove();
        });

        $('[data-success-modal]').html(data.message);
        $('.modal').modal('hide');
        $("#successModal").modal('show');
    }

    function showFailedModal(data, form, show) {
        $(form).find('.help-block').remove();
        $(form).find('.error-input').removeClass('error-input');

        $.each(data.message, function( key, value ) {
            var input = $(form).find('[name='+key+']');
            input.addClass('error-input');
            input.parent().append('<span class="help-block">' +
                '            <i class="fa fa-exclamation-circle" aria-hidden="true"></i> <strong>'+value+'</strong>' +
                '</span>');
            $("ul.error-message").html('').append('<li><i class="fa fa-close text-danger"></i>'+value+'</li>');
        });
        if($(form).attr('data-form-withdrawal')) return;
        console.log($(form).attr('data-form-auth'));
        if(!show) return;
        $("#failedModal").modal('show');
    }

});