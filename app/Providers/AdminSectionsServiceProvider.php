<?php

namespace App\Providers;

use App\User;
use SleepingOwl\Admin\Contracts\Widgets\WidgetsRegistryInterface;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\User::class => 'App\Admin\Http\Sections\Users',
        \App\Role::class => 'App\Admin\Http\Sections\Role',
        \App\Orders::class => 'App\Admin\Http\Sections\Orders',
        \App\Withdrawal::class => 'App\Admin\Http\Sections\Paid',
        \App\Contact::class => 'App\Admin\Http\Sections\Contacts',
        \App\Donate::class => 'App\Admin\Http\Sections\Donate',
    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {

        $this->app->call([$this, 'registerViews']);
        parent::boot($admin);
    }


    /**
     * @var array
     */
    protected $widgets = [
        \Admin\Widgets\NavigationUserBlock::class,
        \Admin\Widgets\Views::class
    ];
    /**
     * @param WidgetsRegistryInterface $widgetsRegistry
     */
    public function registerViews(WidgetsRegistryInterface $widgetsRegistry)
    {
        foreach ($this->widgets as $widget) {
            $widgetsRegistry->registerWidget($widget);
        }
    }
}
