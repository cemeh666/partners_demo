<?php

namespace App\Providers;

use App\Settings;
use App\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        \AdminFormElement::add('email', \App\Form\Email::class);
        \Validator::extend('float', function ($attribute, $value, $parameters, $validator) {
            $result = preg_match('/^\d*(\.\d*)?$/',$value);
            return $result;
        });
        \Validator::extend('balance', function ($attribute, $value, $parameters, $validator) {
            $model = $validator->getData();
            $user = User::where('id', $model['user_id'])->first();
            return $user->balance >= $value && $value >= Settings::first()->min_paid;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
