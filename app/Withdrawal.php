<?php

namespace App;

use App\Mail\EmailVerification;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Withdrawal extends \Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'withdrawal';
    protected $guarded = ['id'];
    public static $type =[
        1 => 'Check',
        2 => 'PayPal',
        3 => 'Donate',
    ];

    public static $status =[
        0 => 'During',
        1 => 'Complete',
    ];

    public static function boot()
    {
        Withdrawal::saved(function (\App\Withdrawal $order) {
            if($order->getOriginal('type') == null){
                if($order->type == 3)
                    \App\BalanceHistory::create_history($order->user_id, 3, $order->count, 'Cash out: '.\App\Donate::getDonateName($order->donate), $order->id);
                else
                    \App\BalanceHistory::create_history($order->user_id, 2, $order->count, 'Cash out: '.\App\Withdrawal::$type[$order->type], $order->id);
            }
        });

        Withdrawal::creating(function (\App\Withdrawal $order) {
            $user = $order->user;
            $user->balance = $user->balance - $order->count;
            $user->save();
        });
    }

    public function Validate($type = 1)
    {
        $rules = [];
        if($type == 1){
            $rules = array(
                'user_id'   => array('required','int'),
                'type'      => array('required','int'),
                'name'      => array('required','max:191'),
                'address'   => array('required','max:191'),
                'city'      => array('required','max:191'),
                'region'    => array('required','max:191'),
                'zip'       => array('required','int'),
                'count'     => array('required','float'),
            );
        }
        if($type == 2){
            $rules = array(
                'user_id'   => array('required','int'),
                'type'      => array('required','int'),
                'email'     => array('required','email'),
                'count'     => array('required','float'),
            );
        }
        if($type == 3){
            $rules = array(
                'name'      => array('required','max:191'),
                'count'     => array('required','float'),
            );
        }

        $model = self::getAttributes();
//        dd($rules, $model);

        $validator = \Validator::make($model, $rules);
        if ($validator->fails()) {
            return $validator;
        }
        return true;
    }


    public static function create_withdrawal($type, $inputs){
//        dd($inputs);
        $withdrawal = new Withdrawal([
            "user_id"   => \Auth::user()->id,
            "type"      => $type,
            "count"     => isset($inputs['count']) ? str_replace(',', '.', $inputs['count']) : '',
            "name"      => isset($inputs['name']) ? $inputs['name'] : '',
            "address"   => isset($inputs['address']) ? $inputs['address'] : '',
            "city"      => isset($inputs['city']) ? $inputs['city'] : '',
            "region"    => isset($inputs['region']) ? $inputs['region'] : '',
            "zip"       => isset($inputs['zip']) ? $inputs['zip'] : '',
            "email"     => isset($inputs['email']) ? $inputs['email'] : '',
            "donate"    => isset($inputs['donate']) ? $inputs['donate'] : '',
            "custom_name"    => isset($inputs['custom_name']) ? $inputs['custom_name'] : '',
            "status"    => 0,
        ]);
        $user = \Auth::user();
        if($withdrawal->Validate($type) === true){
            if($withdrawal['count'] > $user->balance) return ['count' => 'You have no sufficient balance to process this transaction.'];
            if($withdrawal['count'] < Settings::first()->min_paid) return ['count' => 'The min request amount to process a transaction is '.\App\Settings::first()->min_paid.'$'];
            if($type == 2){
                $user->paypal = $inputs['email'];
                $user->save();
            }
            try{
                $subject = 'Cash out Confirmation from Loyal Partners';
                \Mail::to(\Auth::user()->email)
                    ->cc('admin@loyalmoving.com')
                    ->queue(new EmailVerification(['pay' => $withdrawal,'user' => \Auth::user(), 'type'=>$type], 'emails.pay_out', $subject));
            }catch (\Exception $e){
                return [$e->getMessage()];
            }
            return $withdrawal->save();
        }
        return $withdrawal->Validate($type)->getMessageBag();
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function donate()
    {
        return $this->belongsTo(Donate::class,'donate','id');
    }
}
