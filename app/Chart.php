<?php

namespace App;

use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Chart extends \Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'chart';
    protected $guarded = ['id'];

    public static function save_chart($inputs){
        $chart = Chart::first();

        if($chart){
            $chart->text = isset($inputs['text']) ? $inputs['text'] : '';
            return $chart->save();
        }
    }
}
