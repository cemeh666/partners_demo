<?php

namespace App;

use App\Mail\EmailVerification;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class TextEditor extends \Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'text_editor';
    protected $guarded = ['id'];

    public static function getText($text_id){
        $text = TextEditor::where('text_id', $text_id)->first();
        return isset($text->text) ? $text->text : '';
    }

    public static function saveText($text_id, $save_text){
            $text = TextEditor::where('text_id', $text_id)->first();
            $text->text = $save_text;
            $text->save();
    }
}
