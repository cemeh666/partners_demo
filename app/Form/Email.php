<?php

namespace App\Form;

use SleepingOwl\Admin\Form\Element\NamedFormElement;

class Email extends NamedFormElement
{
    public function __construct($path, $label = null)
    {
        parent::__construct($path, $label);

        $this->setHtmlAttributes([
            'class' => 'form-control',
            'type'  => 'email',
        ]);
    }

    /**
     * @var string
     */
    protected $view = 'form.element.text';
}
