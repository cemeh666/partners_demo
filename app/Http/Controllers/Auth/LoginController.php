<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function auth($inputs)
    {
       return $this->guard()->attempt(['email'=>$inputs['email'], 'password' => $inputs['password']], true);
    }
    public function showLoginForm(Request $request)
    {
        if(!$request->session()->get('url.intended') || stristr($request->session()->get('url.intended'), 'admin') === false)
            return redirect('/');
        return view('auth.login');

    }
}
