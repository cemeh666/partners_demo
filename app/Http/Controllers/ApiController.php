<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Mail\EmailVerification;
use App\Orders;
use App\User;
use App\Withdrawal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Plivo\RestAPI;

class ApiController extends Controller
{
    public function registration(Request $request)
    {
        $result = User::regApi($request->all(), \Session::get('invite'));
        if($result === true){
            return $this->sendSuccess('You have successfully signed up! <br> Check your e-mail to confirm');
        }
        return $this->sendError($result);
    }
    public function promo(Request $request)
    {
        if($request->type_send && is_array($request->type_send)){
            foreach ($request->type_send as $send){
                if($send == 1 && $request->email){
                    try{
                        \Mail::to($request->email)
                            ->queue(new EmailVerification(['text'=>$request->text], 'emails.promo', 'This is '.\Auth::user()->name.', please use...'));
                    }catch (\Exception $e){
                        return [$e->getMessage()];
                    }
                }
                if($send == 2 && $request->phone){
                    $auth_id = "MAMJM3ODCZMTDINTGXMJ";
                    $auth_token = "NDYzM2JjNDZiOTZmMmE5ZTUyYmRhOGY0MmNjNDgz";

                    $p = new RestAPI($auth_id, $auth_token);

                    // Set message parameters
                    $params = array(
                        'src' => '+1 213-214-0482', // Sender's phone number with country code
                        'dst' => $request->phone, // Receiver's phone number with country code
                        'text' => $request->text, // Your SMS text message
                        'url' => 'https://www.loyalmoving.com', // The URL to which with the status of the message is sent
                        'method' => 'POST' // The method used to call the url
                    );
                    // Send message
                    $response = $p->send_message($params);
                    $message = '';

                    \Log::useFiles(storage_path('/logs/smsStatus.log'));
                    // Print the response
                    $message .= "Response : ";

                    // Print the Api ID
                    $message .= "\n Api ID : {$response['response']['api_id']}";

                    // Print the Message UUID
                    $message .=  "\n Message UUID : {$response['response']['message_uuid'][0]}";
                    $message .=  "\n Message : {$response['response']['message']}";
                    \Log::info($message);
                }
            }
        }
        return $this->sendSuccess('You have successfully sent Your Promo Code');
    }

    public function auth(Request $request)
    {
        $result = new LoginController();
        $result = $result->auth($request->all());
        if($result){
            return $this->sendSuccess('You have successfully enter', '/profile');
        }
        return $this->sendError(['email'=>'Login or password was incorrect']);
    }

    public function auth_email(Request $request)
    {
        $result = new LoginController();
        $result = $result->auth($request->all());
        if($result){
            return redirect('/profile');
        }
        return redirect('/')->with('errorMessage', ['Login or password was incorrect']);
    }

    public function reset_password(Request $request)
    {
        $user= User::where('email', $request->reset_email)->first();
        if($user){
            $token = \Password::broker()->createToken($user);
            $result = $user->reset_password($token);
            if($result === true){
                return $this->sendSuccess('We have emailed a link to reset your password');
            }
            return $this->sendError(['reset_email'=>'Oops! Something went wrong']);

        }
        return $this->sendError(['reset_email'=>'Email not match']);
    }

    public function withdrawal($type, Request $request)
    {
        $result = Withdrawal::create_withdrawal($type, $request->all());
        if($result === true){
            return $this->sendSuccess('Your request has been submitted', '/withdrawal');
        }
        return $this->sendError($result);
    }
    public function order_update($id, Request $request){
        $order = Orders::where('user_id', \Auth::user()->id)->where('id', $id)->first();
        if($order){
            if(in_array($order->status, Orders::$no_editable))  return $this->sendSuccess('The information has been updated successfully.', '/profile');
            $result = Orders::update_order($order, $request->all());
            if($result === true){
                return $this->sendSuccess('The information has been updated successfully.', '/profile');
            }
            return $this->sendError($result);
        }
        return $this->sendError(['Order not found']);
    }

    public function promo_order($promo, Request $request){
        $user = User::where('promo', strtoupper($promo))->first();
        $inputs = $request->all();
        if($user){
            Orders::create_order([
                "info" => 'Quote',
                "name" => isset($inputs['name']) ? $inputs['name'] : '',
                "email" => isset($inputs['email']) ? $inputs['email'] : '',
                "phone" => isset($inputs['phone']) ? $inputs['phone'] : '',
                "size" => isset($inputs['size']) ? $inputs['size'] : '',
                "from" => isset($inputs['from']) ? $inputs['from'] : '',
                "to" => isset($inputs['to']) ? $inputs['to'] : '',
                "date" => isset($inputs['date']) ? $inputs['date'] : Carbon::now()->toDateString(),
            ], $user->id);
            return $this->sendSuccess();
        }
        return $this->sendError(['promo'=>'not found']);
    }
    public function insurance(Request $request){
      if($request->name && $request->address){
          try{
              \Mail::to('admin@loyalmoving.com')
                  ->queue(new EmailVerification(['name'=>$request->name,'address'=>$request->address], 'emails.insurance', 'Additional Insured Request'));
          }catch (\Exception $e){
              return [$e->getMessage()];
          }
      }
        return $this->sendSuccess('Thank you!<br>Your request has been submitted.');

    }
}
