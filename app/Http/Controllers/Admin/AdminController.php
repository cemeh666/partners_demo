<?php

namespace App\Http\Controllers\Admin;

use App\BalanceHistory;
use App\Chart;
use App\Http\Controllers\Controller;
use App\Insurance;
use App\Invites;
use App\Settings;
use Illuminate\Http\Request;
/**
 * Class ContestController
 * @package App\Http\Controllers\Admin
 */
class AdminController extends Controller
{
    public function settings(Request $request)
    {
        $result = Settings::save_settings($request->all());
        if($result === true){
             return back()->with('success_message', 'Settings save');
        }
        return back()->with('error_message', implode('<br>', $result));
    }

    public function insurance(Request $request)
    {
        $result = Insurance::save_insurance($request->all());
        if($result === true){
             return back()->with('success_message', 'Insurance save');
        }
        return back()->with('error_message', 'Insurance not found');
    }

    public function insurance_delete($id)
    {
        $result = Insurance::delete_insurance($id);
        if($result === true){
            return back()->with('success_message', 'Insurance save');
        }
        return back()->with('error_message', 'Insurance not found');
    }

    public function chart(Request $request)
    {
        $result = Chart::save_chart($request->all());
        if($result === true){
             return back()->with('success_message', 'Chart save');
        }
        return back()->with('error_message', 'Chart not found');
    }

    public static function enter($user_id){
        \Auth::loginUsingId($user_id);
        return redirect('/profile');
    }

    public static function balance($user_id){
        $history = BalanceHistory::where('user_id', $user_id)->get();
        return \AdminSection::view(view('admin.user.balance', compact('history')), 'User balance');
    }
}
