<?php

namespace App\Http\Controllers;

use App\BalanceHistory;
use App\Chart;
use App\Contact;
use App\Insurance;
use App\Invites;
use App\Orders;
use App\Settings;
use \App\User;
use App\Withdrawal;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $user = \Auth::user();
        $invite = $request->invite ? $request->invite : \Session::get('invite');
        $partners = User::count();
        $cache = Withdrawal::sum('count');
        $donate = 0;
        if($invite){
            \Session::put('invite', $invite);
            \Session::save();
        }
        $settings = Settings::first();
        if($settings){
            $partners += $settings->count_partners;
            $cache += $settings->count_cache;
            $donate += $settings->count_donate;
        }
        return view('index', compact('user', 'invite', 'partners', 'cache', 'donate'));
    }

    public function profile(Request $request)
    {
        $search = $date = '';
        $status = 'show all';
        $user = \Auth::user();
        $orders = Orders::where('user_id', $user->id);
        if($request->search){
            $orders = $orders->where('name', 'LIKE', '%'.$request->search.'%');
            $search = $request->search;
        }
        if($request->date_order){
            $date = explode('-', $request->date_order);
            if(count($date) > 1){
                try{
                    $orders = $orders
                        ->where('created_at','>=', Carbon::createFromFormat('m.d.Y', trim($date[0]))->toDateString())
                        ->where('created_at','<=',  Carbon::createFromFormat('m.d.Y', trim($date[1]))->toDateString());
                }catch (\Exception $exception){
                    $orders = $orders
                        ->where('created_at','>=', Carbon::parse(trim($date[0]))->toDateString())
                        ->where('created_at','<=', Carbon::parse(trim($date[1]))->toDateString());
                }
            }else{
                try{
                    $orders = $orders->where('date', Carbon::createFromFormat('m.d.Y', $request->date_order)->toDateString());
                }catch (\Exception $exception){
                    $orders = $orders->where('date', Carbon::parse($request->date_order)->toDateString());
                }
            }

            $date = $request->date_order;
        }

        if($request->status || $request->status == 0){
            if($request->status != -1 && $request->status != null){
                $orders = $orders->where('status', $request->status);
                $status = Orders::$status[$request->status];
            }else{
                $orders = $orders->where('status', '<>', 2);
            }
        }
        $status_id = $request->status;
        $orders = $orders->get();

        return view('profile.index', compact('user', 'orders', 'search', 'date', 'status', 'status_id'));
    }

    public function profile_order(Request $request)
    {
        $result = Orders::create_order($request->all());
        if($result === true){
            return $this->sendSuccess('The information has been successfully submitted.','/profile');
        }
        return $this->sendError($result);
    }

    public function settings()
    {
        $user = \Auth::user();
        return view('profile.settings', compact('user'));
    }

    public function settings_save(Request $request)
    {
        if($request->password == null){
            unset($request['password']);
        }
        \Auth::user()->phone ? $redirect = '/settings' : $redirect = '/profile';
        $this->validate($request, User::$settings_rules);
        $result = User::settings_save($request->all());
        if($result === true){
            return $this->sendSuccess('The password has been successfully updated.', $redirect);
        }
        return $this->sendError();
    }

    public function invites(Request $request)
    {
        $search = $date = '';
        $status = 'show all';
        $user = \Auth::user();
        $invites = Invites::where('user_id', $user->id);
        if($request->search){
            $invites = $invites->where('email', 'LIKE', '%'.$request->search.'%');
            $search = $request->search;
        }
        if($request->date){
            try{
                $invites = $invites
                    ->where('created_at','>=', Carbon::createFromFormat('m.d.Y', $request->date)->startOfDay()->toDateTimeString())
                    ->where('created_at','<=', Carbon::createFromFormat('m.d.Y', $request->date)->endOfDay()->toDateTimeString());
            }catch (\Exception $exception){
                $invites = $invites
                    ->where('created_at','>=', Carbon::parse($request->date)->startOfDay()->toDateTimeString())
                    ->where('created_at','<=', Carbon::parse($request->date)->endOfDay()->toDateTimeString());
            }

            $date = $request->date;
        }
        $status_id = $request->status;

        if($request->status || $request->status == 0){
            if($request->status != -1 && $request->status != null){
                $invites = $invites->where('status', $request->status);
                $status = Invites::$status[$request->status];
            }
        }

        $invites = $invites->get();
        if(empty($user->phone) || empty($user->name) || empty($user->address))
            return redirect('/settings');
        return view('profile.invites',  compact('user', 'invites', 'search', 'date', 'status', 'status_id'));
    }

    public function send_invite(Request $request){

        $result = Invites::create_invite($request->all());
        if($result === true){
            return back()->with('successMessage', 'The invitation has been sent.');
        }
        return back()->with('errorMessage', $result)->withInput($request->all());
    }

    public function send_contact(Request $request){

        $result = Contact::create_contact($request->all());
        if($result === true){
            return back()->with('successMessage', 'Thank you for your message, we will contact you shortly.');
        }
        return back()->with('errorMessage', $result)->withInput($request->all());
    }

    public function chart(){
        $user = \Auth::user();
        $chart = Chart::first();

        return view('chart', compact('user', 'chart'));
    }
    public function insurance(){
        $user = \Auth::user();
        $insurance = Insurance::first();

        return view('insurance', compact('user', 'insurance'));
    }

    public function order_archive($id){
        $user = \Auth::user();
        $order = Orders::where('id', $id)->where('user_id', $user->id)->first();
        if($order){
            $order->update(['status' => 2]);
            return back()->with('successMessage', 'Successfully archived.');
        }

        return back()->with('errorMessage', 'Not found');
    }

    public function order_restore($id){
        $user = \Auth::user();
        $order = Orders::where('id', $id)->where('user_id', $user->id)->first();
        if($order){
            $order->update(['status' => 0]);
            return back()->with('successMessage', 'The info has been restored from archive.');
        }

        return back()->with('errorMessage', 'Not found');
    }

    public function withdrawal(Request $request){
        $user = \Auth::user();
        $search = $date = '';
        $status = 'show all';
        $history = BalanceHistory::where('user_id', $user->id);

        if($request->search){
            $history = $history->where('name', 'LIKE', '%'.$request->search.'%');
            $search = $request->search;
        }

        if($request->date_order){
            try{
                $history = $history
                    ->where('created_at','>=', Carbon::createFromFormat('m.d.Y', $request->date_order)->startOfDay()->toDateTimeString())
                    ->where('created_at','<=', Carbon::createFromFormat('m.d.Y', $request->date_order)->endOfDay()->toDateTimeString());
            }catch (\Exception $exception){
                $history = $history
                    ->where('created_at','>=', Carbon::parse($request->date_order)->startOfDay()->toDateTimeString())
                    ->where('created_at','<=', Carbon::parse($request->date_order)->endOfDay()->toDateTimeString());
            }

            $date = $request->date_order;
        }

        if($request->status || $request->status == 0){
            if($request->status != -1 && $request->status != null){
                $history = $history->where('type', $request->status);
                $status = BalanceHistory::$type[$request->status];
            }
        }
        $history = $history->get();
        $status_id = $request->status;

        return view('withdrawal', compact('user', 'search', 'date', 'status', 'history', 'status_id'));

    }

    protected function buildFailedValidationResponse(Request $request, array $errors)
    {
        return $this->sendError($errors);
    }
}
