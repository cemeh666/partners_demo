<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendSuccess($message = null, $redirect = null){
        return response(json_encode([
            'status' => 'Ok',
            'message' => $message ? $message : "",
            'redirect' => $redirect ? $redirect : "",
        ]));
    }
    public function sendError($message = null){
        return response(json_encode([
            'status'  => 'Error',
            'message' => $message ? $message : []
        ]));
    }
}
