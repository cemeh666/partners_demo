<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class Views
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(stristr(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '', 'admin') === false){
            \App\Views::add(Carbon::now()->toDateString());
        }
        return $next($request);

    }
}
