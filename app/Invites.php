<?php

namespace App;

use App\Mail\EmailVerification;

class Invites extends \Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'invites';
    protected $guarded = ['id'];
    public static $status = [
        0 => 'Sent',
        1 => 'Accepted',
        2 => 'Completed'
    ];

    public function Validate()
    {
        $rules = array(
            'email'     => array('required','email','max:191'),
        );

        $model = self::getAttributes();
//        dd($rules, $model);

        $validator = \Validator::make($model, $rules);
        if ($validator->fails()) {
            return $validator;
        }
        return true;
    }

    public static function create_invite($inputs){

        $invite= new Invites([
            "email" => isset($inputs['email']) ? $inputs['email'] : '',
            "user_id" => \Auth::user()->id,
            "status" => 0,
            "paid" => 0,
        ]);

        if($invite->Validate() === true){
            if(Invites::where('email', $invite['email'])->where('user_id', $invite['user_id'])->exists())
                return ['You have already invited this person'];
            if($invite['email'] == \Auth::user()->email)
                return ['You can not invite yourself'];
            try{
                $link = env('APP_URL').'/?invite='.\Auth::user()->id;
                $subject = "Invitation from ".\Auth::user()->name;
                \Mail::to($invite['email'])
                    ->queue(new EmailVerification(['link' => $link, 'user'=>\Auth::user()], 'emails.invite', $subject));
            }catch (\Exception $e){
                return [$e->getMessage()];
            }
            return $invite->save();
        }
        return $invite->Validate()->getMessageBag()->all();

    }

   public static function save_referral($referral, $user_id){
        $invite = Invites::where('email', $referral->email)
            ->where('user_id', $user_id)->first();
        if($invite){
            $invite->status = 1;
            $invite->save();
        }else{
            Invites::create([
                "email" => $referral->email,
                "user_id" => $user_id,
                "status" => 1,
                "send" => 1,
            ]);
        }
   }

   public static function paid_referral($friend_email){
        $invite = Invites::where('email', $friend_email)->where('status', 1)->where('paid', 0)->first();
        if($invite){
            $result = \App\BalanceHistory::create_history($invite->user_id, 1, Settings::first()->price_invite, 'Referral: '.$friend_email);
            if($result === true){
                $invite->update(['paid'=> 50, 'status'=>2]);
                User::where('id', $invite->user_id)->increment('balance', Settings::first()->price_invite);
            }
            else
                dd($result);

        }
   }
}
