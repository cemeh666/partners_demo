<?php

namespace App;

use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Insurance extends \Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'insurance';
    protected $guarded = ['id'];

    public static function save_insurance($inputs){
        $insurance = Insurance::first();
        if($insurance){
            $insurance->text = isset($inputs['text']) ? $inputs['text'] : '';
            $insurance->name1 = isset($inputs['name1']) ? $inputs['name1'] : '';
            $insurance->name2 = isset($inputs['name2']) ? $inputs['name2'] : '';
            $insurance->name3 = isset($inputs['name2']) ? $inputs['name3'] : '';


            $patch = public_path('/ins');
            if(\Request::hasFile('file1')){
                $file_name = time().\Request::file('file1')->hashName();
                try{
                    $inputs['file1']->move($patch, $file_name);
                    \File::delete(public_path($insurance->file1));

                    $insurance->file1 = '/ins/'.$file_name;

                }catch(FileException $e){
                    return [$e->getMessage()];
                }
            }
            if(\Request::hasFile('file2')){
                $file_name = time().\Request::file('file2')->hashName();
                try{
                    $inputs['file2']->move($patch, $file_name);
                    \File::delete(public_path($insurance->file2));

                    $insurance->file2 = '/ins/'.$file_name;

                }catch(FileException $e){
                    return [$e->getMessage()];
                }
            }
            if(\Request::hasFile('file3')){
                $file_name = time().\Request::file('file3')->hashName();
                try{
                    $inputs['file3']->move($patch, $file_name);
                    \File::delete(public_path($insurance->file3));

                    $insurance->file3 = '/ins/'.$file_name;

                }catch(FileException $e){
                    return [$e->getMessage()];
                }
            }

            return $insurance->save();

        }
    }
    public static function delete_insurance($id){
        $insurance = Insurance::first();
        if($insurance){
            \File::delete(public_path($insurance["file$id"]));

            $insurance["name$id"] = '';
            $insurance["file$id"] = '';
            return $insurance->save();
        }
    }
}
