<?php

namespace App;

class Donate extends \Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'donate';
    protected $guarded = ['id'];

    public static function getDonate(){
        $donate = Donate::where('active', true)->get();
        return $donate;
    }

    public static function getDonateName($id){
        $donate = Donate::where('id', $id)->first();
        if($donate) return $donate->name;
        return '';
    }

}
