<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailVerification extends Mailable
{
    use Queueable, SerializesModels;


    public function __construct($data, $view, $subject)
    {
        $this->data = $data;
        $this->view = $view;
        $this->subject = $subject;
    }
    protected $data;

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;

        if(isset($data['order']->file)){
            $file_name = explode('.', $data['order']->file);
            if(isset($file_name[0])){
                return $this->view($this->view)->with($data)->subject($this->subject)
                    ->attach(env('APP_URL').$data['order']->file, ['as' => 'Inventory-List.'.$file_name[0]]);
            }

        }

        return $this->view($this->view)->with($data)->subject($this->subject);
    }
}
