<?php
/**
 * Created by PhpStorm.
 * User: cemeh666
 * Date: 26.04.17
 * Time: 13:41
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
class Role extends Model
{
    protected $fillable = [
        'name', 'label',
    ];
    /**
     * A user may have multiple roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function setModelsAttribute($models)
    {
        if(empty($models)) return $this->attributes['models'] = null;
        $this->attributes['models'] = json_encode($models);
    }

    public function getModelsAttribute($models)
    {
        if($models && !is_array($models))
           return $this->attributes['models'] = json_decode($models, true);
        if($this->attributes['models'] == null) return [];
        return $this->attributes['models'];

    }

    public function setPagesAttribute($model)
    {
        if(empty($model)) return $this->attributes['pages'] = null;

        $this->attributes['pages'] = json_encode($model);
    }

    public function getPagesAttribute($model)
    {
        if($model && !is_array($model))
            return $this->attributes['pages'] = json_decode($model, true);
        if($this->attributes['pages'] == null) return [];

        return $this->attributes['pages'];
    }
}