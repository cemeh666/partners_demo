<?php

namespace App;

use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use App\Mail\EmailVerification;

class Orders extends \Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'orders';
    protected $guarded = ['id'];
    public static $status = [
        0 => 'Submitted',
        1 => 'Completed',
        2 => 'Archive',
        3 => 'Pending',
        4 => 'Declined',
        5 => 'Reserved'
    ];
    public static $no_editable = [
        1,2,4
    ];
    public static function boot()
    {
        Orders::saving(function (\App\Orders $order) {
            if($order->work && $order->amount && $order->commission){
                $order->balance = $order->amount*($order->commission/100);
                $history = \App\BalanceHistory::where('user_id', $order->user->id)->where('order_id', $order->id)->where('type', 1)->first();
                if($order->status == 1){
                    if(!$history){
                        $result = \App\BalanceHistory::create_history($order->user->id, 1, $order->balance, $order->name, $order->id);
                        if($result === true)
                            $order->user->increment('balance', $order->balance);
                        else
                            dd($result);
                    }else{
                        $order->user->decrement('balance', $history->count);
                        $history->delete();
                        $result = \App\BalanceHistory::create_history($order->user->id, 1, $order->balance, $order->name, $order->id);
                        if($result === true)
                            $order->user->increment('balance', $order->balance);
                        else
                            dd($result);
                    }

                }
            }
            if($order->status == 1){
                \App\Invites::paid_referral($order->user->email);
            }
        });
    }
    public function Validate()
    {
        $rules = array(
            'email'    => array('required','email','max:191'),
            'name'     => array('max:191'),
            'info'     => array('required', 'max:191'),
            'phone'    => array('required', 'max:191'),
            'size'     => array('max:191'),
            'from'     => array('max:191'),
            'to'       => array('max:191'),
            'date'     => array('max:191'),
            'time'     => array('max:191'),
            'address'  => array('max:191'),
            'file'     => array('max:'.Settings::max_upload(),'mimes:jpeg,jpg,pdf,etc,xls,xlsx,doc,docx'),
        );

        $model = self::getAttributes();
//        dd($rules, $model);

        $validator = \Validator::make($model, $rules);
        if ($validator->fails()) {
            return $validator;
        }
        return true;
    }

    public function ValidateUpdate()
    {
        $rules = array(
            'email'    => array('required','email','max:191'),
            'name'     => array('max:191'),
            'info'     => array('max:191'),
            'phone'    => array('required', 'max:191'),
            'size'     => array('max:191'),
            'from'     => array('max:191'),
            'to'       => array('max:191'),
            'date'     => array('max:191'),
            'time'     => array('max:191'),
            'address'  => array('max:191'),
            'file'     => array('max:'.Settings::max_upload(),'mimes:jpeg,jpg,pdf,etc,xls,xlsx,doc,docx'),
        );

        $model = self::getAttributes();
//        dd($rules, $model);

        $validator = \Validator::make($model, $rules);
        if ($validator->fails()) {
            return $validator;
        }
        return true;
    }

    public static function create_order($inputs, $user_id = null){

        $order = new Orders([
            "user_id" => $user_id ? $user_id : \Auth::user()->id,
            "status" => 0,
            "balance" => 0,
            "info" => isset($inputs['info']) ? $inputs['info'] : '',
            "name" => isset($inputs['name']) ? $inputs['name'] : '',
            "email" => isset($inputs['email']) ? $inputs['email'] : '',
            "copy" => isset($inputs['copy']) ? true : false,
            "phone" => isset($inputs['phone']) ? $inputs['phone'] : '',
            "size" => isset($inputs['size']) ? $inputs['size'] : '',
            "from" => isset($inputs['from']) ? $inputs['from'] : '',
            "to" => isset($inputs['to']) ? $inputs['to'] : '',
            "time" => isset($inputs['time']) ? $inputs['time'] : '',
            "address" => isset($inputs['address']) ? $inputs['address'] : '',
            "file" => isset($inputs['file']) ? $inputs['file'] : '',
        ]);
        try{
            $order['date'] = isset($inputs['date']) ? Carbon::parse($inputs['date'])->toDateString() : null;
        }catch (\Exception $exception){
            $order['date'] = isset($inputs['date']) ? Carbon::createFromFormat('m.d.Y', $inputs['date'])->toDateString() : null;
        }

        if($order->Validate() === true){
            if(\Request::hasFile('file')){
                $patch = public_path('/users_files');
                $file_name = time().\Request::file('file')->hashName();
                try{
                    $inputs['file']->move($patch, $file_name);
                    $order->file = '/users_files/'.$file_name;

                }catch(FileException $e){
                    return [$e->getMessage()];
                }
            }
            if($order->copy){
                $subject = "$order->info confirmation from Loyal Partners";
                try{
                    \Mail::to(\Auth::user()->email)
                        ->queue(new EmailVerification(['order' => $order,'user' => \Auth::user()], 'emails.order', $subject));
                }catch (\Exception $e){
                    return [$e->getMessage()];
                }
            }
            $subject = \Auth::user()->name." create order from Loyal Partners";
            try{
                \Mail::to('partners@loyalmoving.com')
                    ->queue(new EmailVerification(['order' => $order,'user' => \Auth::user()], 'emails.order', $subject));
            }catch (\Exception $e){
                return [$e->getMessage()];
            }
            return $order->save();
        }
        return $order->Validate()->getMessageBag();

    }

    public static function update_order(Orders $order, $inputs){

        $order->file = isset($inputs['file']) ? $inputs['file'] : '';
        $order->email = isset($inputs['email']) ? $inputs['email'] : '';
        $order->phone = isset($inputs['phone']) ? $inputs['phone'] : '';
        $order->address = isset($inputs['address']) ? $inputs['address'] : '';
        $order->name = isset($inputs['name']) ? $inputs['name'] : '';
        $order->size = isset($inputs['size']) ? $inputs['size'] : '';
        $order->from = isset($inputs['from']) ? $inputs['from'] : '';
        $order->to = isset($inputs['to']) ? $inputs['to'] : '';
        $order->time = isset($inputs['time']) ? $inputs['time'] : '';
        $order->address = isset($inputs['address']) ? $inputs['address'] : '';
        try{
            $order->date = isset($inputs['date']) ? Carbon::parse($inputs['date'])->toDateString() : Carbon::now()->toDateString();
        }catch (\Exception $exception){
            $order->date = isset($inputs['date']) ? Carbon::createFromFormat('m.d.Y', $inputs['date'])->toDateString() : Carbon::now()->toDateString();
        }
        if($order->ValidateUpdate() === true){
            if(\Request::hasFile('file')){
                $patch = public_path('/users_files');
                $file_name = time().\Request::file('file')->hashName();
                try{
                    $inputs['file']->move($patch, $file_name);
                    $order->file = '/users_files/'.$file_name;

                }catch(FileException $e){
                    return [$e->getMessage()];
                }
            }else
                unset($order->file);

            return $order->save();
        }
        return $order->ValidateUpdate()->getMessageBag();
    }

    public static function getTime(){
        $time = [];
        foreach (range(0, 23) as $item){
            $time[] = $item.' : 00';
        }
        return $time;
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
