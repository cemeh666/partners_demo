<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{

    use HandlesAuthorization;

    protected $rules = [
        'moder' => ['App\Admin\Http\Sections\Users']
    ];

    /**
     * @param User   $user
     * @param string $ability
     *
     * @return bool
     */
    public function before(User $user, $ability, $section, $item = null)
    {
        if ($ability != 'display' && $ability != 'create' && !is_null($item) && $item->id <= 1) {
            return false;
        }
        if($user->isSuperAdmin()) return true;

            if (!in_array($section->getClass(), $user->getModels())) {
                return false;

            }
            return true;

    }

}