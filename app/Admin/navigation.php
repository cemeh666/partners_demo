<?php


return [
    [
        'title' => 'Dashboard',
        'icon'  => 'fa fa-dashboard',
        'url'   => route('admin.dashboard'),
        'priority' => 50,
//        'accessLogic' => function (Page $page) {
//            return $page->isActive();
//      },
    ],
    [
        'title' => 'Insurance',
        'icon'  => 'fa fa-cog settings',
        'url'   => '/admin/insurance',
        'priority' => 400,

    ],
    [
        'title' => 'Chart',
        'icon'  => 'fa fa-cog settings',
        'url'   => '/admin/chart',
        'priority' => 400,
    ],
    [
        'title' => 'Settings',
        'icon'  => 'fa fa-cog settings',
        'url'   => '/admin/settings',
        'priority' => 500,

    ],


];