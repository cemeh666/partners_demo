<?php

namespace App\Admin\Http\Sections;

use App\User;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;



/**
 * Class Users
 *
 * @property \App\User $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Users extends Section implements Initializable
{
    /**
     * Initialize class.
     */
    public function initialize()
    {
        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 51, function() {

            return User::count();
        })->setIcon('fa fa-users');

        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            //...
        });
    }
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Users';

    /**
     * @var string
     */
    protected $alias = 'users';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
        ->setHtmlAttribute('class', 'table-primary')
            ->with('roles')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::image('avatar', 'Photo<br/><small>(image)</small>')
                    ->setHtmlAttribute('class', 'hidden-sm hidden-xs foobar')
                    ->setWidth('100px'),
                AdminColumn::link('name', 'Имя')->setWidth('200px')->setLinkAttributes(['target' => '_blank']),
                AdminColumn::email('email', 'Email'),
                AdminColumn::text('phone', 'Phone'),
                AdminColumn::lists('roles.label', 'Roles')->setWidth('200px'),
                AdminColumn::custom('Balance', function ($instance) {
                    return "<a href='/admin/users/$instance->id/balance' target='_blank'>$instance->balance</a>";
                }),
                AdminColumn::custom('Enter', function ($instance) {
                    return "<a href='/admin/users/$instance->id/enter' target='_blank' class='btn btn-default'><i class='fa fa-user-secret'></i> </a>";
                })
            )->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $columns = AdminFormElement::columns([
            [
                AdminFormElement::text('name', 'Имя')->required(),
            ],
            [
                AdminFormElement::email('email', 'Email')->required()->addValidationRule('email')->addValidationRule('unique:users'),
            ],
            [
                AdminFormElement::password('password', 'Password')->required(),
            ]
        ]);
        return AdminForm::panel()->addBody($columns);

    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);

    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
