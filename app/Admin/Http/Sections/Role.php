<?php

namespace App\Admin\Http\Sections;

use App\RoleModels;
use App\User;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;

/**
 * Class Role
 *
 * @property \App\Role $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Role extends Section implements Initializable
{

    public function initialize()
    {
//        $this->addToNavigation($priority = 500, function() {
//            return \App\Role::count();
//        });

    }

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('label', 'label')->setWidth('30px'),
                AdminColumn::link('name', 'Name')->setWidth('200px')
            )->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
       $role = \App\Role::where('id', $id)->first();
        $pages = \AdminNavigation::getPages();
        $model = $page = $selected_page = [];
        foreach ($pages as $key => $item){
            if($item->hasModel())
                $model[$key] =  $key;
            else{
                $page[$key] = $item->getTitle();
                if(is_array($role->pages) && in_array($key, $role->pages))
                    $selected_page[$key] = $item->getTitle();
            }
        }

        $columns = AdminFormElement::columns([
            [
                AdminFormElement::text('name', 'Имя')->required(),
                AdminFormElement::multiselect('models', 'Models', $model)->setDefaultValue($role->models)
            ],
            [
                AdminFormElement::text('label', 'label'),
                AdminFormElement::multiselect('pages', 'Pages', $page)->setDefaultValue($selected_page)
            ]
        ]);
        return AdminForm::panel()->addBody($columns);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
//        return $this->onEdit(null);
        $pages = \AdminNavigation::getPages();
        $model = $page = [];
        foreach ($pages as $key => $item){
            if($item->hasModel())
                $model[$key] =  $key;
            else
                $page[$key] = $item->getTitle();

        }
        $columns = AdminFormElement::columns([
            [
                AdminFormElement::text('name', 'Имя')->required(),
                AdminFormElement::multiselect('models', 'Models', $model)
            ],
            [
                AdminFormElement::text('label', 'label'),
                AdminFormElement::multiselect('pages', 'Pages', $page)
            ]
        ]);
        return AdminForm::panel()->addBody($columns);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
