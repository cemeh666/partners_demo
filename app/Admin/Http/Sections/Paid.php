<?php

namespace App\Admin\Http\Sections;

use App\User;
use App\Withdrawal;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;



/**
 * Class Users
 *
 * @property \App\User $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Paid extends Section implements Initializable
{
    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation($priority = 53, function() {

            return \App\Withdrawal::count();
        })->setIcon('fa fa-money');

        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            //...
        });
    }
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Paid out';

    /**
     * @var string
     */
    protected $alias = 'paid';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
        ->setHtmlAttribute('class', 'table-primary')
            ->with('user')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('user.name', 'User'),
                AdminColumn::text('count', 'Count'),
                AdminColumn::custom('Type', function ($instance) {
                    return Withdrawal::$type[$instance->type];
                }),
                AdminColumn::custom('Status', function ($instance) {
                    return $instance->status ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-close text-danger"></i>';
                })
            )->paginate(20);
    }
//AdminColumn::custom('Status', function ($instance) {
//    return $instance->active ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>';
//})->setHtmlAttribute('class', 'text-center')->setWidth('50px')
    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $columns = AdminFormElement::columns([
            [
                AdminFormElement::select('type', 'Type', Withdrawal::$type)->required(),
                AdminFormElement::text('name', 'Name'),
                AdminFormElement::text('custom_name', 'Custom Name'),
                AdminFormElement::email('email', 'Email'),
                AdminFormElement::select('user_id', 'User', User::pluck('email','id')->toArray())->required()
            ],
            [
                AdminFormElement::text('address', 'Address'),
                AdminFormElement::text('region', 'Region'),
                AdminFormElement::text('city', 'City'),
                AdminFormElement::text('zip', 'ZIP'),
            ],
            [
                AdminFormElement::text('count', 'Count')->required(),
                AdminFormElement::select('donate', 'Donate', \App\Donate::pluck('name','id')->toArray()),
                AdminFormElement::select('status', 'Status', Withdrawal::$status)->required(),
            ]
        ]);
        return AdminForm::panel()->addBody($columns);

    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $columns = AdminFormElement::columns([
            [
                AdminFormElement::select('type', 'Type', Withdrawal::$type)->required(),
                AdminFormElement::text('name', 'Name'),
                AdminFormElement::email('email', 'Email'),
                AdminFormElement::select('user_id', 'User', User::pluck('email','id')->toArray())->required()
            ],
            [
                AdminFormElement::text('address', 'Address'),
                AdminFormElement::text('region', 'Region'),
                AdminFormElement::text('city', 'City'),
                AdminFormElement::text('zip', 'ZIP'),
            ],
            [
                AdminFormElement::text('count', 'Count')->required()->addValidationRule('balance'),
                AdminFormElement::select('status', 'Status', Withdrawal::$status)->required(),
            ]
        ]);
        return AdminForm::panel()->addBody($columns);

    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
