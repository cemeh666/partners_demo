<?php

namespace App\Admin\Http\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
/**
 * Class Donate
 *
 * @property \App\Donate $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Donate extends Section implements Initializable
{
    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation($priority = 60, function() {

            return \App\Donate::count();
        })->setIcon('fa fa-money');

        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            //...
        });
    }
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Donate company';

    /**
     * @var string
     */
    protected $alias = 'donate';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Name'),
                AdminColumn::image('image', 'Image'),
                AdminColumn::custom('Active', function ($instance) {
                    return $instance->active ? "<i class='fa fa-check'></i>" :  "<i class='fa fa-close'></i>";
                })
            )->paginate(20);    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $columns = AdminFormElement::columns([
            [
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::checkbox('active', 'Active'),
            ],
            [
                AdminFormElement::image('image', 'Image')->required()->addValidationRule('max:'.\App\Settings::max_upload()),
            ]
        ]);
        return AdminForm::panel()->addBody($columns);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
