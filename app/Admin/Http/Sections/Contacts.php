<?php

namespace App\Admin\Http\Sections;

use App\User;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;



/**
 * Class Users
 *
 * @property \App\User $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Contacts extends Section implements Initializable
{
    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation($priority = 55, function() {

            return \App\Contact::where('read', 0)->count();
        })->setIcon('fa fa-envelope-o');

        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            //...
        });
    }
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Contacts';

    /**
     * @var string
     */
    protected $alias = 'contacts';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
        ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Name'),
                AdminColumn::text('phone', 'Phone'),
                AdminColumn::custom('Read', function ($instance) {
                    return $instance->read ? "<i class='fa fa-check'></i>" :  "<i class='fa fa-close'></i>";
                })
            )->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $columns = AdminFormElement::columns([
            [
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::text('phone', 'Phone')->required(),
                AdminFormElement::text('address', 'address')->required(),
            ],
            [
                AdminFormElement::textarea('comments', 'Comment')->required(),
            ],
            [
                AdminFormElement::checkbox('read', 'Read')
            ]
        ]);
        return AdminForm::panel()->addBody($columns);

    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);

    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
