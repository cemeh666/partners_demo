<?php

namespace App\Admin\Http\Sections;

use App\Settings;
use App\User;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;



/**
 * Class Users
 *
 * @property \App\User $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Orders extends Section implements Initializable
{
    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation($priority = 52, function() {

            return \App\Orders::count();
        })->setIcon('fa fa-cubes');

        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            //...
        });
    }
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Orders';

    /**
     * @var string
     */
    protected $alias = 'orders';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
        ->setHtmlAttribute('class', 'table-primary')
            ->with('user')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::link('info', 'Info'),
                AdminColumn::link('name', 'Name'),
                AdminColumn::email('email', 'Email'),
                AdminColumn::text('phone', 'Phone'),
                AdminColumn::text('user.name', 'User'),
                AdminColumn::custom('Status', function ($instance) {
                    return \App\Orders::$status[$instance->status];
                }),
                AdminColumn::text('balance', 'Balance')
            )->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $columns = AdminFormElement::columns([
            [
                AdminFormElement::text('name', 'Name'),
                AdminFormElement::email('email', 'Email')->required(),
                AdminFormElement::text('size', 'Size'),
                AdminFormElement::text('from', 'From'),
                AdminFormElement::text('to', 'TO'),
                AdminFormElement::select('user_id', 'User', User::pluck('email','id')->toArray()),
                AdminFormElement::textarea('address', 'NOTES'),
            ],
            [
                AdminFormElement::text('info', 'Info'),
                AdminFormElement::text('phone', 'Phone')->required(),
                AdminFormElement::date('date', 'Date'),
                AdminFormElement::text('time', 'Time'),
                AdminFormElement::file('file', 'File')->addValidationRule('max:'.\App\Settings::max_upload()),
            ],
            [
                AdminFormElement::text('work', 'Time Work')->addValidationRule('nullable')->addValidationRule('integer'),
                AdminFormElement::text('amount', 'Customer Paid')->addValidationRule('nullable')->addValidationRule('integer'),
                AdminFormElement::text('customer_saved', 'Customer Saved')->addValidationRule('nullable')->addValidationRule('integer'),
                AdminFormElement::text('commission', 'Commission %')->addValidationRule('nullable')->addValidationRule('integer'),
                AdminFormElement::select('status', 'Status', \App\Orders::$status),
                AdminFormElement::file('check', 'Check file')->addValidationRule('max:'.\App\Settings::max_upload()),
                AdminFormElement::textarea('comment', 'LOYAL MOVING NOTES'),

            ]
        ]);
        return AdminForm::panel()->addBody($columns);

    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);

    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
