<?php

//Route::get('', ['as' => 'admin.dashboard', function () {
//	$content = view('admin.dashboard');
//	return AdminSection::view($content, 'Dashboard');
//}]);

Route::get('information', ['as' => 'admin.information', function () {
	$content = 'Define your information here.';
	return AdminSection::view($content, 'Information');
}]);

Route::get('settings', ['as' => 'admin.settings', function () {
    $settings = \App\Settings::first();
    return AdminSection::view(view('admin.settings', compact('settings')), 'Settings');
}]);

Route::get('insurance', ['as' => 'admin.insurance', function () {
    $insurance = \App\Insurance::first();
    return AdminSection::view(view('admin.insurance', compact('insurance')), 'Insurance');
}]);

Route::get('chart', ['as' => 'admin.chart', function () {
    $chart = \App\Chart::first();
    return AdminSection::view(view('admin.chart', compact('chart')), 'COMMISSION CHART');
}]);
Route::group(['prefix' => '', 'namespace'  => 'App\Http\Controllers\Admin', 'middleware'=>'auth'], function() {
    Route::post('settings', 'AdminController@settings');
    Route::post('insurance', 'AdminController@insurance');
    Route::post('chart', 'AdminController@chart');
    Route::get('/users/{id}/enter', 'AdminController@enter');
    Route::get('/users/{id}/balance', 'AdminController@balance');
    Route::get('/insurance/{id}/delete', 'AdminController@insurance_delete');

});
