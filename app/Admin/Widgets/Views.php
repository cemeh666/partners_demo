<?php
namespace Admin\Widgets;
use AdminTemplate;
use App\User;
use SleepingOwl\Admin\Widgets\Widget;
class Views extends Widget
{
    /**
     * Get content as a string of HTML.
     *
     * @return string
     */
    public function toHtml()
    {
        return view('admin.views', [
            'user' => auth()->user(),
            'users_count' => User::count(),
        ])->render();
    }
    /**
     * @return string|array
     */
    public function template()
    {
        return AdminTemplate::getViewPath('dashboard');
    }
    /**
     * @return string
     */
    public function block()
    {
        return 'block.top';
    }
}