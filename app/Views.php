<?php
/**
 * Created by PhpStorm.
 * User: cemeh666
 * Date: 26.04.17
 * Time: 13:41
 */

namespace App;

use Carbon\Carbon;

class Views extends \Eloquent
{
    public $timestamps = false;

    protected $fillable = [
        'date', 'views',
    ];

    public static function add($date){
        $record = Views::where('date', $date)->first();
        if($record){
            $record->increment('views');
        }else{
            Views::create([
                'date' => $date,
                'views' => 1,
            ]);
        }
    }

    public static function getLastWeek(){
        $records = Views::orderBy('id', 'desc')->take(7)->get();
        $data = '[';
        foreach ($records as $item){

            $data .= '["'.Carbon::parse($item->date)->format('d M').'",'.$item->views.'],';
        }
        $data .= ']';
        return $data;

    }
}