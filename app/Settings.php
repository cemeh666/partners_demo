<?php

namespace App;

use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Settings extends \Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'settings';
    protected $guarded = ['id'];


    public function Validate()
    {
        $rules = array(
            'count_partners' => array('int'),
            'count_cache'    => array('int'),
            'count_donate'   => array('int'),
            'min_paid'       => array('int'),
        );

        $model = self::getAttributes();
//        dd($rules, $model);

        $validator = \Validator::make($model, $rules);
        if ($validator->fails()) {
            return $validator;
        }
        return true;
    }


    public static function save_settings($inputs){
        $settings = Settings::first();
        if($settings){
            $settings->count_partners = isset($inputs['count_partners']) ? $inputs['count_partners'] : '';
            $settings->count_cache    = isset($inputs['count_cache']) ? $inputs['count_cache'] : '';
            $settings->count_donate   = isset($inputs['count_donate']) ? $inputs['count_donate'] : '';
            $settings->min_paid       = isset($inputs['min_paid']) ? $inputs['min_paid'] : '';
            $settings->price_invite   = isset($inputs['price_invite']) ? $inputs['price_invite'] : '';
        }

        if($settings->Validate() === true){

            return $settings->save();
        }
        return $settings->Validate()->getMessageBag()->all();

    }


    public static function max_upload()
    {
        $upload_max_size = ini_get('upload_max_filesize');

        $val  = trim($upload_max_size);

        $last = strtolower($val[strlen($val)-1]);
        $val  = substr($val, 0, -1); // necessary since PHP 7.1; otherwise optional

        switch($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }

        return $val;
    }

}
