<?php

namespace App;

use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class BalanceHistory extends \Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'balance_history';
    protected $guarded = ['id'];
    public static $type = [
        1 => 'Deposit',
        2 => 'Cash out',
        3 => 'Donated',
    ];

    public function Validate()
    {
        $rules = array(
            'user_id'     => array('required'),
            'name'     => array('required', 'max:191'),
            'type'     => array('required'),
            'count'     => array('required'),
        );

        $model = self::getAttributes();
//        dd($rules, $model);

        $validator = \Validator::make($model, $rules);
        if ($validator->fails()) {
            return $validator;
        }
        return true;
    }


    public static function create_history($user_id, $type, $count, $name, $order_id = null){

        $history = new BalanceHistory([
            "user_id" => $user_id,
            "type" => $type,
            "count" => $count,
            "name" => $name,
            "order_id" => $order_id,
        ]);

        if($history->Validate() === true){

            return $history->save();
        }
        return $history->Validate()->getMessageBag();

    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
