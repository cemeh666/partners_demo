<?php

namespace App;

use App\Mail\EmailVerification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class User extends Authenticatable
{
    use HasRoles, Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $settings_rules = [
        'name' => ['required','max:191'],
        'phone' =>['required','max:191'],
        'address' => ['required','max:191'],
        'city' => ['required','max:191'],
        'zip' => ['required','integer'],
        'state' => ['required','max:191'],
        'paypal' => ['max:191'],
        'delivery' => ['max:191'],
        'other' => ['max:191'],
        'avatar' => ['image', 'max:5000','mimes:jpeg,png'],
        'password'  => ['min:3','confirmed'],
    ];

    public static function boot()
    {
        User::deleting(function (\App\User $user) {
            return false;
        });


    }

    public function Validate()
    {
        $rules = array(
            'email'     => array('required','email','max:191','unique:users'),
            'name'     => array('max:191'),
        );

        $model = self::getAttributes();
//        dd($rules, $model);

        $validator = \Validator::make($model, $rules);
        if ($validator->fails()) {
            return $validator;
        }
        return true;
    }


    public static function regApi($inputs, $invite){
        $password = self::generatePassword();
        $user = new User([
            'email'     => isset($inputs['email']) ? $inputs['email'] : '',
            'name'      => isset($inputs['name'])  ? $inputs['name'] : '',
            'password'  => $password,
            'balance'   => 0,
            'promo'     => strtoupper('LM'.self::generatePassword(8))
        ]);
        if($user->Validate() === true){
            try{
                $link = env('APP_URL').'/email/auth?email='.$user->email.'&password='.$password;

                \Mail::to($user->email)
                    ->queue(new EmailVerification(['link' => $link, 'password' => $password, 'email'=>$user->email], 'emails.sign-up', 'Success registration Partners'));
            }catch (\Exception $e){
              return [$e->getMessage()];
            }
            $user->save();
            if($invite)
                Invites::save_referral($user, $invite);
            return true;
        }
        return $user->Validate()->getMessageBag();
    }

    public function reset_password($token){

            try{
                $link = env('APP_URL')."/password/reset/$token";

                \Mail::to($this->email)
                    ->queue(new EmailVerification(['link' => $link], 'emails.reset', 'Password Reset - LoyalMoving Partners'));
//                });
                return true;
            }catch (\Exception $e){
                return [$e->getMessage()];
            }
    }

    public static function settings_save($inputs){
        $user = \Auth::user();
        $user->name = $inputs['name'];
        $user->phone = $inputs['phone'];
        $user->address = $inputs['address'];
        $user->city = $inputs['city'];
        $user->zip = $inputs['zip'];
        $user->state = $inputs['state'];
        $user->paypal = isset($inputs['paypal']) ? $inputs['paypal'] : null;
        $user->check = isset($inputs['delivery']) ? $inputs['delivery'] : null;
        if(isset($inputs['password']))
            $user->password = $inputs['password'];

        if(isset($inputs['specialist'])){
            $user->other = null;
            $user->specialist = $inputs['specialist'];
        }
        if($inputs['other']){
            $user->other = $inputs['other'];
            $user->specialist = null;
        }
//dd($inputs, $user);
        if(\Request::hasFile('avatar')){
            $patch = public_path('/images/users_avatar');
            $file_name = time().\Request::file('avatar')->hashName();
            $src = $patch.'/'.$file_name;

            try{
                ini_set('memory_limit', '-1');
                $data = explode(',', $inputs['avatar_crop']);
                if(isset($data[1])){
                    $data = base64_decode($data[1]);
                    \File::put($src, $data);
                    $user->avatar = '/images/users_avatar/'.$file_name;
                }
            }catch(FileException $e){
                return [$e->getMessage()];
            }
        }
        return $user->save();
    }


    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->hasRole('admin');
    }
    /**
     * @return bool
     */
    public function isManager()
    {
        return $this->hasRole('manager');
    }

    public function setPasswordAttribute($password)
    {
        if( $password )
            $this->attributes['password'] = bcrypt($password);
    }

    public function getModels(){
        $roles = \Auth::user()->roles;
        $models = [];
        foreach ($roles as $role) {
            $models = array_merge($models, $role->models);
        }
        return array_unique($models);
    }

    public function checkModels($model){
        if(\Auth::user()->isSuperAdmin()) return true;
       return in_array($model, $this->getModels());
    }

    public function getPages(){
        $roles = \Auth::user()->roles;
        $pages = [];
        foreach ($roles as $role) {
            $pages = array_merge($pages, $role->pages);
        }
        return array_unique($pages);
    }

    public function getAvatar(){

        return $this->avatar ?  $this->avatar : '/images/front/default_avatar.png';
    }
    public function getID(){
        $id = (string)($this->id);
        $count = strlen($id);
        switch ($count) {
            case 1:
                $id = '000-00-0'.$id;
                break;
            case 2:
                $id = '000-00-'.$id;
                break;
            case 3:
                $id = '000-0'.$id[0].'-'.$id[1].$id[2];
                break;
            case 4:
                $id = '000-'.$id[0].$id[1].'-'.$id[2].$id[3];
                break;
            case 5:
                $id = '00'.$id[0].'-'.$id[1].$id[2].'-'.$id[3].$id[4];
                break;
            case 6:
                $id = '0'.$id[0].$id[1].'-'.$id[2].$id[3].'-'.$id[4].$id[5];
                break;
            case 7:
                $id = $id[0].$id[1].$id[2].'-'.$id[3].$id[4].'-'.$id[5].$id[6];
                break;
        }
//        000-00-01
        return $id;
    }

    private static function generatePassword($length = 8){
        $chars = 'acdefghjkmnpqrstuvwxyzACDEFGHJKMNPQRSTUVWXYZ2345679';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }
}
