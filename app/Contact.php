<?php

namespace App;

use App\Mail\EmailVerification;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Contact extends \Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'contact';
    protected $guarded = ['id'];

    public function Validate()
    {
        $rules = array(
            'name'     => array('required','max:191'),
            'address'     => array('required','max:191','email'),
            'phone'     => array('required','max:191'),
            'comments'     => array('required','max:5000'),
        );

        $model = self::getAttributes();
//        dd($rules, $model);

        $validator = \Validator::make($model, $rules);
        if ($validator->fails()) {
            return $validator;
        }
        return true;
    }

    public static function create_contact($inputs){

        $contact = new Contact([
            "name" => isset($inputs['name']) ? $inputs['name'] : '',
            "address" => isset($inputs['address']) ? $inputs['address'] : '',
            "phone" => isset($inputs['phone']) ? $inputs['phone'] : '',
            "comments" => isset($inputs['comments']) ? $inputs['comments'] : '',
            "read" => 0,
        ]);

        if($contact->Validate() === true){
            try{
                $subject = 'New contacts from Loyal Partners';
                \Mail::to('admin@loyalmoving.com')
                    ->queue(new EmailVerification(['contacts' => $contact], 'emails.contacts', $subject));
            }catch (\Exception $e){
                return [$e->getMessage()];
            }
            return $contact->save();
        }
        return $contact->Validate()->getMessageBag()->all();
    }

}
